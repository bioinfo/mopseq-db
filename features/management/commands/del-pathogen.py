from django.core.management.base import BaseCommand, no_translations
from features.models import Pathogen

class Command(BaseCommand):
    help = (
        "Deletes all instances from Pathogen table. "
        "Requires to use the --delete argument otherwise "
        "only DB size content is printed out."
    )
    
    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            "--delete",
            action="store_true",
            help="Execute the objects deletion")
    
    @no_translations
    def handle(self, *args, **options):
        pInstances = Pathogen.objects.count()
        print(f'Current content of database: {pInstances} Pathogen objects')
        if options["delete"]:
            Pathogen.objects.all().delete()
            pInstances = Pathogen.objects.count()
            print(f'After deletion content of database: {pInstances} Pathogen objects')
        else:
            print("Nothing deteled. Use --delete to force.")
