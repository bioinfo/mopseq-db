#########################################################################################
# This script update the database 
# Author: Jean-Christophe Mouren, Clémentine Battistel, Maude Jacquot
# Creation Date: 2022-05-28
# Last modified: 2024-02-23
#########################################################################################

from django.core.management.base import BaseCommand, no_translations
from features.models import Pathogen
import re, argparse
import colorama
from colorama import Fore
from colorama import Style

#Data type verification
isInt=re.compile(r'^[0-9]+$')
isDate=re.compile(r'^[1-2][0-9]{3}[\-][0-1][0-9][\-][0-3][0-9]$')
dateAttribut=["collection_date", "time_release", "date_release"] # Have to be a date
intAttribut=["collection_year", "nb_reads", "seq_coverage", "seq_size", "nb_contigs"] #Have to be a integer
kingdom=["Viruses", "Bacteria", "Protozoa"]

def handleDataRow(sample, newvalues, att_list):
    error=False
    for cmpt, att in enumerate(att_list):
        if not hasattr(sample, att):
            print(Fore.YELLOW+f'    WARNING: attribute [{att}] does not exist in DB schema: discarded'+Style.RESET_ALL)
            continue
        val=newvalues[cmpt]
        if att in dateAttribut and val:
            if re.match(isDate, val):
                setattr(sample, att, val)
            else:
                print(Fore.RED+f'    ERROR: invalid format for date: {att}: {val}'+Style.RESET_ALL)
                error=True
        elif att in intAttribut and val:
            if re.match(isInt, val):
                setattr(sample, att, val)
            else:
                print(Fore.RED+f'    ERROR: invalid format for integer: {att}: {val}'+Style.RESET_ALL)
                error=True
        elif att == "kingdom" and val:
            if val in kingdom:
                setattr(sample, att, val)
            else:
                print(Fore.RED+f'   ERROR: kingdom not expected: {att}: {val}: not one of {kingdom}'+Style.RESET_ALL)
                error=True
        else : 
            if val:
                setattr(sample, att, val)
    return not error

class Command(BaseCommand):
    """
    A command to update MoPSeq-DB database. Metadata format is verified before database addition. If the sample_name of a genome is not already save in the database, a new Pathogen instance is created, otherwise, the already existing instance is updated if --update option is used.
    """
    help = (
        "Add instances in Pathogen table, or update existing instances."
    )
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('file', type=argparse.FileType('r'), help="A csv file containting metadata of genome you want to add to MoPSeq-DB. You can find a template in mopseq-db/static/other/MoPSeq-DB_template.csv")    #read the file passed in argument
        # Named (optional) arguments
        parser.add_argument("--update", action='store_true', help="To update data of already existing genomes in the database")

    @no_translations
    def handle(self, *args, **options):
        colorama.init()

        sample_names=dict() #to verify if this sample is not already recorded in the database

        nbline=0
        added_genomes=0 #number of added genomes
        updated_genomes=0 #number of already existing sample
        error_genomes=0 #number of genomes with format error

        error=False

        for row in options["file"]:
            if "sample_name" in row:
                sep=row[11] # get the separator of the csv file (can be ',' or ';')
                att_list=row.rstrip("\n").split(sep)
            else :
                nbline+=1
                newvalues=row.rstrip("\n").split(sep) 

                #if newvalues[0] not in sample_names :
                sample_name_line=newvalues[att_list.index("sample_name")]
                pathogen_line=newvalues[att_list.index("organism")]
                print(f'> Import {pathogen_line}:{sample_name_line}')
                if pathogen_line not in sample_names.keys() :
                    sample_names[pathogen_line]=Pathogen.objects.filter(organism=pathogen_line).order_by().values_list("sample_name", flat=True).distinct() 

                if sample_name_line not in sample_names[pathogen_line]:
                    sample=Pathogen() #An empty Pathogen object
                    print(f'  new entry')
                    if handleDataRow(sample, newvalues, att_list):
                        sample.save()
                        added_genomes+=1
                    else:
                        error_genomes+=1
                        error = True
                else : 
                    if options["update"] :
                        sample=Pathogen.objects.filter(sample_name=sample_name_line)[0]
                        if handleDataRow(sample, newvalues, att_list):
                            sample.save()
                            updated_genomes+=1
                        else:
                            error_genomes+=1
                            error = True
                    else : 
                        print(sample_name_line+" is already recorded in MoPSeq-DB. To update its metadata use --update option.")
        
        print(f"Your file contains {nbline} genomes:")
        print(f"  - {added_genomes} genomes have been added to MoPSeq-DB database")
        if options["update"] :
            print(f"  - {updated_genomes} genomes have been updated")
        if error:
            print(f"  - {error_genomes} genomes contains data formatting error, NOT loaded into the database")
            print(Fore.YELLOW+"\nYour csv file contains field with unsupported metadata type. Please make sure your data are in the right format, as follows:\n")
            print("\t- kingdom field must be one of "+str(kingdom)+"\n\t- fields "+str(intAttribut)+" must be intergers\n\t- fields "+str(dateAttribut)+" must be dates in YYYY-MM-dd format.\n\n"+Style.RESET_ALL)
        print(f'Your database contains {Pathogen.objects.count()} genomes.')
