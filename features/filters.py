#########################################################################################
# This script contains data filtering functions called in view_table_pat 
# Author: Jean-Christophe Mouren, Clémentine Battistel, Maude Jacquot
# Creation Date: 2022-05-28
# Last modified: 2024-02-20
#########################################################################################

from django_filters import *
from features.models import Pathogen

def filtre(**kwargs):

    request = kwargs.pop('request')

    queryset = kwargs.pop('queryset') 

    organism = kwargs.pop('organism') 

    def filtered_val(attribut, organism):  
        CHOICES=[]
        for i in list(Pathogen.objects.filter(organism=organism).values_list(attribut, flat=True).distinct().order_by(attribut)):  #On récupère un Queryset des différentes noms de pays uniques et par ordre alphabétique qu'on transforme en liste sous la forme liste = [(pays,pays),(pays,pays)]  <- c'est le format obligatoire de l'option CHOICES de django_filter FilterChoice
            if i == None:
                continue
            else:
                CHOICES.append((i,i))
        return CHOICES

    class patFilter(FilterSet):
        #Changer taille box de filtre widget=forms.TextInput(attrs= {'size': 15,}))
        host = ChoiceFilter(field_name='host', label="Host species", choices=filtered_val('host', organism))

        host_stage = ChoiceFilter(field_name='host_stage', choices=filtered_val('host_stage', organism))

        collection_year = ChoiceFilter(field_name='collection_year', choices=filtered_val('collection_year', organism))
        collection_year2 = NumberFilter(field_name='collection_year', lookup_expr='gt') # 'gt' Pour faire année plus grand que..
        collection_year3 = NumberFilter(field_name='collection_year', lookup_expr='lt') # 'lt' Pour faire année plus petite que..

        pool_individual = ChoiceFilter(field_name='pool_individual', label="Pool or Individual", choices=filtered_val('pool_individual', organism))

        sample_conservation_method = ChoiceFilter(field_name='sample_conservation_method', choices=filtered_val('sample_conservation_method', organism))

        sequencing_technology = ChoiceFilter(field_name='sequencing_technology', choices=filtered_val('sequencing_technology', organism))

        genome_type = ChoiceFilter(field_name='genome_type', choices=filtered_val('genome_type', organism))
       
        assembly = ChoiceFilter(field_name='assembly', choices=filtered_val('assembly', organism))

        country_sample = ChoiceFilter(field_name='country_sample', label="Country of origin", choices=filtered_val('country_sample', organism))

        nature_coordinates = ChoiceFilter(field_name='nature_coordinates', label="Coordinates precision", choices=filtered_val('nature_coordinates', organism))

        isolation_source = ChoiceFilter(field_name='isolation_source', label="Isolation source", choices=filtered_val('isolation_source', organism))

        sequencer = ChoiceFilter(field_name='sequencer', label="Sequencer", choices=filtered_val('sequencer', organism))

        organization = ChoiceFilter(field_name='organization', choices=filtered_val('organization', organism))

        project_name = ChoiceFilter(field_name='project_name', label="Associated project", choices=filtered_val('project_name', organism))

        nb_reads = NumberFilter(field_name='nb_reads', label="Number of reads is greater than", lookup_expr='gt') 

        nb_contig = NumberFilter(field_name='nb_contigs', label="Maximum number of contigs", lookup_expr='lt') 

        seq_coverage = NumberFilter(field_name='seq_coverage', label="Sequence coverage is greater than", lookup_expr='gt') 
        
        class Meta:
            model = Pathogen
            fields = {'host', 'collection_year', 'country_sample', 'host_stage', 'pool_individual', 'sample_conservation_method', 'sequencing_technology', 'genome_type', 'assembly', 'nature_coordinates', 'isolation_source', 'sequencer', 'organization', 'project_name', 'nb_contigs' }
    
            #fields = '__all__' si on veut un filtre sur tous les champs 
            #exclude = ['last_name', 'organization', 'project_title','doi','biosample_id',]

    return patFilter(request, queryset)