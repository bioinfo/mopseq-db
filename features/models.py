from django.db import models

# This file defines the data model (used to setup SQL schema)
# Note: blank attribute set to true by default, enabling empty value
        
class Pathogen(models.Model):
    class GenbankIdType(models.TextChoices):
        TBD = 'TBD', 'To Be Determined'
        SRA = 'SRA', 'Sequence Read Archive'
        GBK = 'GBK', 'Genbank Core Nucleotide'
        GNM = 'GNM', 'Genomes'

    sample_name = models.fields.CharField(max_length=100, null=True)
    genbank_id = models.fields.CharField(max_length=100, null=True)
    genbank_id_type = models.CharField(max_length=8, choices=GenbankIdType.choices, default=GenbankIdType.TBD)
    strain = models.fields.CharField(max_length=100, null=True)
    isolate = models.fields.CharField(max_length=150, null=True)
    host = models.fields.CharField(max_length=100, null=True)
    collection_year = models.IntegerField(null=True)
    collection_date = models.DateField(null=True, blank=True)
    country_sample = models.fields.CharField(max_length=100, null=True)
    geo_loc_name = models.fields.CharField(max_length=100, null=True)
    latitude = models.fields.CharField(max_length=100, null=True)
    longitude = models.fields.CharField(max_length=100, null=True)
    nature_coordinates = models.fields.CharField(max_length=100,  null=True)
    host_stage = models.fields.CharField(max_length=100, null=True)
    isolation_source = models.fields.CharField(max_length=100, null=True)
    pool_individual = models.fields.CharField(max_length=100, null=True)
    sample_conservation_method = models.fields.CharField(max_length=100, null=True)
    sequencing_technology = models.fields.CharField(max_length=100, null=True)
    sequencer = models.fields.CharField(max_length=100, null=True)
    additional_info_sequencing = models.fields.CharField(max_length=100, null=True)
    nb_reads = models.IntegerField(null=True)
    seq_coverage = models.IntegerField(null=True)
    seq_size = models.IntegerField(null=True)
    viral_charge = models.fields.CharField(max_length=100, null=True)
    assembly = models.fields.CharField(max_length=100, null=True)
    genome_type = models.fields.CharField(max_length=100, null=True)
    structure = models.fields.CharField(max_length=100, null=True)
    first_name = models.fields.CharField(max_length=100,  null=True)
    last_name = models.fields.CharField(max_length=100,  null=True)
    email = models.fields.CharField(max_length=100,  null=True)
    collected_by = models.fields.CharField(max_length=100,  null=True)
    organization = models.fields.CharField(max_length=100,  null=True)
    department = models.fields.CharField(max_length=100,  null=True)
    country_organization = models.fields.CharField(max_length=100,  null=True)
    city = models.fields.CharField(max_length=100,  null=True)
    street = models.fields.CharField(max_length=100,  null=True)
    postal_code = models.fields.CharField(max_length=100,  null=True)
    project_data_type = models.fields.CharField(max_length=100,  null=True)
    sample_score = models.fields.CharField(max_length=100,  null=True)
    project_title = models.fields.CharField(max_length=200,  null=True)
    public_description = models.fields.CharField(max_length=1000,  null=True)
    ncbi_project = models.fields.CharField(max_length=16,  null=True)
    bioproject_id = models.fields.CharField(max_length=16,  null=True)
    biosample_id = models.fields.CharField(max_length=100,  null=True)
    date_release = models.DateField(null=True)
    organism = models.fields.CharField(max_length=100,  null=True)
    kingdom = models.fields.CharField(max_length=100,  null=True)
    doi = models.fields.CharField(max_length=100, null=True)
    notes = models.fields.CharField(max_length=100, null=True)
    project_name = models.fields.CharField(max_length=200, null=True)
    nb_contigs = models.IntegerField(null=True)

    def __str__(self):
        """String for representing the Model object (in Admin site etc.)"""
        return f'{self.sample_name} ({self.organism})'

    def getUrlForGenbankIdType(self):
        """Return the canonical NCBI URL to use given type (a GenbankIdType)"""
        match self.genbank_id_type:
            case Pathogen.GenbankIdType.SRA:
                return f'https://www.ncbi.nlm.nih.gov/biosample/?term={self.genbank_id}'
            case Pathogen.GenbankIdType.GBK:
                return f'https://www.ncbi.nlm.nih.gov/nuccore/{self.genbank_id}'
            case Pathogen.GenbankIdType.GNM:
                return f'https://www.ncbi.nlm.nih.gov/datasets/genome/{self.genbank_id}'
            case _:
                return f'https://www.ncbi.nlm.nih.gov/search/all/?term={self.genbank_id}'
