from django.contrib import admin
from .models import Pathogen

# Register your models here.
admin.site.register(Pathogen)
