from django.template import Library
from features.models import Pathogen
from collections import OrderedDict

register = Library()
#### les tags te servent à faire des raccourcis, définir des reqûete préalables pour que tu n'es plus qu'à appeler le tag dans ton code  
@register.simple_tag
def pat_index():
    liste_pat = Pathogen.objects.order_by().values('kingdom','organism').distinct() #On récupère tous les objets du pathogène dans la base /// utilisé l.71 dans base.html
    liste_pat = reversed(liste_pat)
    dic = OrderedDict()
    for i in liste_pat:
        values = i.values()
        values_iterator = iter(values)
        kingdom_value = next(values_iterator)
        organism_value = next(values_iterator)

        if kingdom_value not in dic and organism_value not in dic:
            dic[kingdom_value] = [organism_value]
        elif kingdom_value in dic and organism_value not in dic:
            dic[kingdom_value].append(organism_value)
        
    return dic

@register.simple_tag
def get_databases_size():
    """
    Return a dictionary with keys = organism names and values = number of genomes in MoPSeq-DB for this organism
    """
    list_object = Pathogen.objects.all()
    list_org = list_object.order_by("organism").values_list("organism", flat=True).distinct()
    db_size=dict()
    for organism in list_org:
        db_size[organism]=len(list_object.filter(organism=organism))
    return db_size

@register.simple_tag
def get_kingdom_size():
    """
    Return a dictionary with keys = kingdom names and values = number of genomes in MoPSeq-DB for this kingdom
    """
    list_object = Pathogen.objects.all()
    list_kingdom= list_object.order_by("kingdom").values_list("kingdom", flat=True).distinct()
    kingdom_size=dict()
    for kingdom in list_kingdom:
        kingdom_size[kingdom]=len(list_object.filter(kingdom=kingdom))
    return kingdom_size

@register.filter
def dictitem(dictionary, key):
    """
    Function to allow to find values of a dictionary thanks to a key
    """
    return dictionary.get(key)

@register.simple_tag
def get_pathogen_type(organism):
    """
    Function that allows to get the kingdom of an organism
    """
    return Pathogen.objects.all().filter(organism=organism)[0].kingdom
