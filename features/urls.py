#########################################################################################################
# This script contains navigation links on MopSeq-DB website
# Author: Jean-Christophe Mouren, Maude Jacquot
# Creation date: 2022-05-28
# Last modified: 2024-02-22
#########################################################################################################
from django.urls import path, re_path
from . import views

app_name = 'features'

urlpatterns = [
    path('', views.views_main.home, name='home'),

    re_path(r'^table_pat/(?P<type_p>[\w-]+)/(?P<organism>[\w\ -]+)/$', views.views_table_pat.table_pat, name='table_pat'),
    re_path(r'^download_table_pat/(?P<type_p>[\w-]+)/(?P<organism>[\w\ -]+)/(?P<listDl>[\w-]+)/(?P<idList>[0-9&]+)/$', views.views_table_pat.download_table_pat, name='download_table_pat'),
 
    re_path(r'^info_pat/(?P<organism>[\w\ -]+)/(?P<sample>[\w.-]+)/$', views.views_info_pat.info_pat, name='info_pat'),
    re_path(r'^download_info_pat/(?P<organism>[\w\ -]+)/(?P<sample>[\w.-]+)/(?P<listDl>[\w-]+)/$', views.views_info_pat.download_info_pat, name='download_info_pat'),

    path('about', views.views_main.about, name='about'),
    path('update', views.views_main.update, name='update'),
    path('design', views.views_main.design, name='design'),
    path('analysis', views.views_main.analysis, name='analysis'),
    path('visu_index', views.views_main.visu_index, name='visu_index'),
    path('table_pat_index', views.views_main.table_pat_index, name='table_pat_index'),
    
    re_path(r'^phylogeny/(?P<organism>[\w\ -]+)/$', views.views_phylo_visu.phylogeny, name='phylogeny'),
    re_path(r'^download_phylo/(?P<organism>[\w\ -]+)/$', views.views_phylo_visu.download_phylo, name='download_phylo'),

    path('test', views.views_main.test, name='test'),
] 