from django.shortcuts import *
from django.apps import apps

# import os module
import os, re
import mimetypes
import fnmatch
from pathlib import Path

#On recupère la variable qui possède le chemin vers le dossier contenant tous les fichiers de data stockés
MPSQ_FILES = os.getenv('MPSQ_FILES')

def find_path(pattern, path): #Fonction pour trouver le fichier que l'on lui passe
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                return os.path.join(root, name) #Il retourne le chemin vers le fichier correspondant

def phylogeny(request, organism):
    context = {'organism': organism}

    PATH_PHYLO = MPSQ_FILES + organism + '/phylo/'

    phylo_path = find_path('*BestModel'+'*', PATH_PHYLO)
    if phylo_path != None :
        phylo_path = Path(phylo_path)
        with open(phylo_path, 'r') as f:
            phylotree =  f.read().rstrip('\n')
            if phylotree is not None:
                context['phylotree'] = phylotree
        f.close()

        phylo_path = find_path('*jmodeltest'+'*', PATH_PHYLO)
        if phylo_path != None :
            phylo_path = Path(phylo_path)
            with open(phylo_path, 'r') as f:
                lines =  f.readlines()
                last = lines[-1]
                first = True
                for line in lines:
                    sep = line.split()
                    if first:
                        jmodel = sep[2]
                        first = False
                    if line.startswith("Arguments"):
                        j_command_full = line[11:] #11 corresponds to the index of the first caracter starting the jmodeltest command in the string
                        replaced = re.sub('\/.*\.phy', 'alignment.phy', j_command_full)
                        j_command = re.sub('\/.*\.out', 'jmodeltest.out', replaced)
                    if line.startswith(" Phyml version"):
                        phyml = sep[3]
                    if line.startswith("phyml "):
                        phyml_command = re.sub('\/.*\.phy', 'alignment.phy', line)
                    if line is last :
                        model = sep[1]
            f.close()
            if jmodel is not None:
                context['jmodel'] = jmodel
            if j_command is not None:
                context['j_command'] = j_command
            if model is not None:
                context['model'] = model
            if phyml is not None:
                context['phyml'] = phyml
            if phyml_command is not None:
                context['phyml_command'] = phyml_command
    else: 
        phylo_path = find_path('*andi'+'*', PATH_PHYLO)
        if phylo_path != None :
            phylo_path = Path(phylo_path)
            tree_generator=""
            alg_free_tool="Andi"
            with open(phylo_path, 'r') as f:
                lines =  f.readlines()
                for line in lines:
                    sep = line.split(" ")
                    if "andi " in line:
                        if "command" not in line:
                            alg_free_version=sep[1]   
                        else:
                            alg_free_command=line[len("andi_command :"):]
                    elif "version : " in line:
                        if tree_generator == "":
                            tree_generator=sep[1][1:]
                            tree_generator_version=sep[-1][:-2]
                        else:
                            tree_generator=tree_generator+" & "+sep[1][1:]
                            tree_generator_version=tree_generator_version+" & "+sep[-1][:-2]
            f.close()
            if alg_free_tool is not None:
                context['alg_free_tool'] = alg_free_tool
            if alg_free_version is not None:
                context['alg_free_version'] = alg_free_version
            if tree_generator is not None:
                context['tree_generator'] = tree_generator
            if tree_generator_version is not None:
                context['tree_generator_version'] = tree_generator_version
            if alg_free_command is not None:
                context['alg_free_command'] = alg_free_command
            #if alg_free_model is not None:
            #    context['alg_free_model'] = = 'JC' #default model on andi --> modif to do
        else:
            print ("No phylogenetic found for: "+organism)

    return render(request, 'phylo_visu.html', context)

def download_phylo(request, organism):
    # Setup the different paths needed
    PATH_PHYLO = MPSQ_FILES + organism + '/phylo/'

    name = "Phyml_BestModel_"+organism+".tree"

    phylo_path = find_path('*BestModel'+'*', PATH_PHYLO)
    path = open(phylo_path, 'rb')

    # Set the mime type
    mime_type, _ = mimetypes.guess_type(phylo_path)
    response = HttpResponse(path, content_type=mime_type)

    # Set the HTTP header for sending to browser
    response['Content-Disposition'] = "attachment; filename=%s" % name
    return response        
