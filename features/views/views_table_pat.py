#########################################################################################
# This script generates the html files containing the vector images of the gff 
# Author: Jean-Christophe Mouren, Clémentine Battistel, Maude Jacquot
# Creation Date: 2022-05-28
# Last modified: 2024-02-20
#########################################################################################

from django.shortcuts import *
from features.models import Pathogen
from features.filters import filtre
from django.apps import apps

# Import mimetypes module
import mimetypes
# import os module
import os
import fnmatch
import zipfile, io
import csv
# Import HttpResponse module
from django.http.response import HttpResponse

#On recupère la variable qui possède le chemin vers le dossier contenant tous les fichiers de data stockés
MPSQ_FILES = os.getenv('MPSQ_FILES')

def table_pat(request, **kwargs): #Le kwarg est là pour récupérer les messages associés à des mots clés qu'on passerait en paramètre de la focntion ou pas (explication : https://waytolearnx.com/2019/04/args-et-kwargs-en-python.html )
    
    type_p = kwargs.get('type_p') #On récupère le nom du type de pathogène
    organism = kwargs.get('organism') #On récupère le nom du pathogène qui est passé en paramète de l'url
    
    liste_objets = Pathogen.objects.filter(organism=organism) #On récupère tous les objets du pathogène dans la base

    myFilter = filtre(request=request.GET, queryset=liste_objets, organism=organism) #On récupère un filtre si l'utlisateur en charge un  
    liste_objets = myFilter.qs #On filtre les objets selon les paramètres du filtre
    
    if 'errormessage' in kwargs : #On vérifie s'il y a un message d'erreur lié un à échec de téléchargement, si oui on l'ajoute au contexte de la page qu'on affiche
        message = kwargs.get('errormessage')
        context = {'type_p':type_p, 'organism': organism, 'liste_objets':liste_objets, 'myFilter':myFilter, 'errormessage' : message}
    else:
        context = {'type_p':type_p, 'organism': organism, 'liste_objets':liste_objets, 'myFilter':myFilter}

    return render(request, 'table_pat.html', context)


def find_path(pattern, path): #Fonction pour trouver le fichier que l'on lui passe
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                return os.path.join(root, name) #Il retourne le chemin vers le fichier correspondant


def download_table_pat(request, type_p, organism, listDl, idList) :
    if listDl != 'zzzz' and idList != '0000': 

        # Setup the different paths needed
        PATH_FASTA_C = MPSQ_FILES + organism + '/fasta/complete/'
        PATH_FASTA_NR = MPSQ_FILES + organism + '/fasta/nr/'
        PATH_FASTA_P = MPSQ_FILES + organism + '/fasta/partial/'
        PATH_GFF_C = MPSQ_FILES + organism + '/gff/files/complete/'
        PATH_GFF_NR = MPSQ_FILES + organism + '/gff/files/nr/'
        PATH_GFF_P = MPSQ_FILES + organism + '/gff/files/partial/'
        PATH_VCF_C = MPSQ_FILES + organism + '/vcf/files/complete/'
        PATH_VCF_NR = MPSQ_FILES + organism + '/vcf/files/nr/'
        PATH_VCF_P = MPSQ_FILES + organism + '/vcf/files/partial/'

        # Get the id of every sample selected
        Un_ID = ''
        listeID = []
        for i in range(len(idList)):
            if idList[i] == '&':
                listeID.append(Un_ID)
                Un_ID = ''
            elif i == len(idList)-1:
                Un_ID += idList[i]
                listeID.append(Un_ID)
            else:
                Un_ID += idList[i]

        # Set up ZIP folder
        zip_filename = "data_folder.zip"
        byte_stream = io.BytesIO()
        zf = zipfile.ZipFile(byte_stream, "w") 

        # We prepare the CSV file if the option was selected
        if "B" in listDl:
            # We open a CSV variable
            metadata_csv = io.StringIO()
            writer = csv.writer(metadata_csv)
            writer.writerow(['sample_name', 'genbank_id', 'strain', 'isolate', 'host', 'collection_year', 'collection_date', 'country_sample', 'geo_loc_name', 'latitude', 'longitude', 'nature_coordinates', 'host_stage', 'isolation_source', 'pool_individual', 'sample_conservation_method', 'sequencing_technology', 'sequencer', 'additional_info_sequencing', 'nb_reads', 'seq_coverage', 'seq_size', 'viral_charge', 'assembly', 'genome_type', 'structure', 'organism', 'doi', 'notes'])

        # Control variable for context
        fail_control_list={}
        for choice in listDl:
            fail_control_list[choice] = False
     
        # We prepare a variable containing the text listing the diverses files not found
        fail_text = ""

        for i in range(len(listeID)):
            queryset = Pathogen.objects.filter(organism=organism, id=listeID[i]).values_list('sample_name', 'genbank_id', 'strain', 'isolate', 'host', 'collection_year', 'collection_date', 'country_sample', 'geo_loc_name', 'latitude', 'longitude', 'nature_coordinates', 'host_stage', 'isolation_source', 'pool_individual', 'sample_conservation_method', 'sequencing_technology', 'sequencer', 'additional_info_sequencing', 'nb_reads', 'seq_coverage', 'seq_size', 'viral_charge', 'assembly', 'genome_type', 'structure', 'organism', 'doi', 'notes')

            if "A" in listDl:
                # Find and extract file path
                filepath1 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_FASTA_C)
                filepath2 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_FASTA_NR)
                filepath3 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_FASTA_P)

                if filepath1 != None :
                    # Open the file for reading content
                    path = open(filepath1, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath1)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath1, "genome/complete/"+queryset[0][0]+'.fasta')  
                    fail_control_list["A"] = False

                if filepath2 != None :
                    # Open the file for reading content
                    path = open(filepath2, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath2)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath2, "genome/nr/"+queryset[0][0]+'_NR.fasta') 
                    fail_control_list["A"] = False
                
                if filepath3 != None :
                    # Open the file for reading content
                    path = open(filepath3, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath3)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath3, "genome/partial/"+queryset[0][0]+'_P.fasta')
                    fail_control_list["A"] = False

                
                #Si le fichier n'est pas existant on relève son nom et on l'ajoute à une chaine de caractère
                if filepath1 is None and filepath2 is None and filepath3 is None :
                        fail_control_list["A"] = True
                        fail_text += "No sequence file found for this sample : "+queryset[0][0]+".\n"

            
            if "B" in listDl:
                # queryset has a list of object with values inside, so we write the whole line in one go
                for value in queryset: 
                    writer.writerow(value)
                
            if "C" in listDl:
                # On trouve le chemin correspondant aux fichiers voulus, la fonction find_path renvoi un chemin en se basant sur une regex avec le sample_name
                filepath1 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_GFF_C)
                filepath2 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_GFF_NR)
                filepath3 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_GFF_P)

                if filepath1 != None :
                    # Open the file for reading content
                    path = open(filepath1, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath1)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath1, "gff/complete/"+queryset[0][0]+'.gff')  
                    fail_control_list["C"] = False

                if filepath2 != None :
                    # Open the file for reading content
                    path = open(filepath2, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath2)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath2, "gff/nr/"+queryset[0][0]+'_NR.gff') 
                    fail_control_list["C"] = False
                
                if filepath3 != None :
                    # Open the file for reading content
                    path = open(filepath3, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath3)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath3, "gff/partial/"+queryset[0][0]+'_P.gff')
                    fail_control_list["C"] = False

                
                #Si le fichier n'est pas existant on relève son nom et on l'ajoute à une chaine de caractère
                if filepath1 is None and filepath2 is None and filepath3 is None :
                        fail_control_list["C"] = True
                        fail_text += "No structure file found for this sample : "+queryset[0][0]+".\n"

        
            if "D" in listDl:
                # On trouve le chemin correspondant aux fichiers voulus, la fonction find_path renvoi un chemin en se basant sur une regex avec le sample_name
                filepath1 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_VCF_C)
                filepath2 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_VCF_NR)
                filepath3 = find_path(queryset[0][0]+'[._-]?[!0-9]*', PATH_VCF_P)

                if filepath1 != None :
                    # Open the file for reading content
                    path = open(filepath1, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath1)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath1, "vcf/complete/"+queryset[0][0]+'.vcf')  
                    fail_control_list["D"] = False

                if filepath2 != None :
                    # Open the file for reading content
                    path = open(filepath2, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath2)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath2, "vcf/nr/"+queryset[0][0]+'_NR.vcf') 
                    fail_control_list["D"] = False
                
                if filepath3 != None :
                    # Open the file for reading content
                    path = open(filepath3, 'rb')
                    # Set the mime type
                    mime_type, _ = mimetypes.guess_type(filepath3)
                    response = HttpResponse(path, content_type=mime_type)
                    # Set the HTTP header for sending normally to browser but here into the ZIP file
                    response['Content-Disposition'] = "attachment; sampleName=%s" % queryset[0][0]
                    # Write the file to the ZIP folder
                    zf.write(filepath3, "vcf/partial/"+queryset[0][0]+'_P.vcf')
                    fail_control_list["D"] = False

                
                #Si le fichier n'est pas existant on relève son nom et on l'ajoute à une chaine de caractère
                if filepath1 is None and filepath2 is None and filepath3 is None :
                        fail_control_list["D"] = True
                        fail_text += "No variation file found for this sample : "+queryset[0][0]+".\n"    

        # We need to return on the CSV at the end of the loop to push only one file
        if "B" in listDl:
            # We set the cursor at the beginning of the file
            metadata_csv.seek(0)
            # Push the CSV into the ZIP
            zf.writestr('metadata.csv', metadata_csv.read())   

        if fail_text != "":
            zf.writestr('not_found.txt', fail_text)

        # Close the zip and setup response
        zf.close()
        response = HttpResponse(byte_stream.getvalue(), content_type="application/x-zip-compressed")
        response['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

        # Send the zip to the user or the error message
        fail_check = True
        for key, value in fail_control_list.items():
            if value == False:
                fail_check = False

        if (fail_check):
            fail_text = fail_text.replace("\n", " ")
            return table_pat(request, type_p=type_p, organism=organism, errormessage=fail_text)
        else:
            return response        
    else:
       return table_pat(request, type_p=type_p, organism=organism)

