from django.shortcuts import *
import os
import logging
from pathlib import Path
from django.conf import settings

logger = logging.getLogger(__name__)

MPSQ_FILES = os.getenv('MPSQ_FILES')

def test(request):
    return render(request, 'test.html', {})

def home(request):
    return render(request, 'home.html', {})
 
def design(design):
    return render(design, 'design.html', {})

def update(request):
    context={}
    context['version'] = settings.MPSQ_VERSION
    if MPSQ_FILES:
        #By design, DB version file is located in the parent dir of data_files one
        dbVersionFile = os.path.join(Path(MPSQ_FILES).parent.absolute(), 'version.txt')
        try:
            with open(dbVersionFile) as f:
                dbVersion = f.read().strip()
        except:
            logger.warning(f'DB Version file not readable: {dbVersionFile}')
            dbVersion = None
        if dbVersion != None:
            context['dbVersion'] = dbVersion
    return render(request, 'update.html', context)

def about(request):
    context={}
    context['version'] = settings.MPSQ_VERSION
    return render(request, 'about.html', context)
    
def analysis(request):
    return render(request, 'analysis.html', {})

def visu_index(request):
    return render(request, 'visu_index.html', {})

def table_pat_index(request):
    return render(request, 'table_pat_index.html', {})
