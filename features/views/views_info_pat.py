from django.shortcuts import *
from features.models import Pathogen
from django.apps import apps
#from django import forms

#from features.forms import ChromosomeChoice

# Import mimetypes module
import mimetypes
# import os module
import os,re
import fnmatch
import zipfile, io
import csv
from pathlib import Path
# Import HttpResponse module
from django.http.response import HttpResponse

#On recupère la variable qui possède le chemin vers le dossier contenant tous les fichiers de data stockés
MPSQ_FILES = os.getenv('MPSQ_FILES')


def find_path(pattern, path): #Fonction pour trouver le fichier que l'on lui passe
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                return os.path.join(root, name) #Il retourne le chemin vers le fichier correspondant

def find_file(pattern, path): #Fonction pour trouver le fichier que l'on lui passe
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                return (name) #Il retourne le nom du fichier

def find_multiple_file(pattern, path): #Fonction pour trouver les fichiers pour le sample qu'on lui passe (pour les organismes qui ont plusieurs chromosomes. A priori que pour les génomes complets du coup non ?)
    list_name=list()
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                list_name.append(name)
    list_name.sort() #To make visualisation choice easier
    return (list_name) #Il retourne les noms des fichiers correspondants

def info_pat(request, **kwargs): #Le kwarg est là pour récupérer les messages associés à des mots clés qu'on passerait en paramètre de la fonction ou pas (explication : https://waytolearnx.com/2019/04/args-et-kwargs-en-python.html )
    
    organism = kwargs.get('organism') #On récupère le nom du pathogène qui est passé en paramète de l'url
    sample = kwargs.get('sample')

    liste_objets =  Pathogen.objects.filter(organism=organism, sample_name=sample) #On récupère tous les attributs du sample dans la base

    context = {'organism': organism, 'sample':sample, 'liste_objets':liste_objets}

    PATH_IMG_GFF_C = MPSQ_FILES + organism + '/gff/img/complete/'
    PATH_IMG_GFF_NR = MPSQ_FILES + organism + '/gff/img/nr/'
    PATH_IMG_GFF_P = MPSQ_FILES + organism + '/gff/img/partial/'
    PATH_IMG_VCF_C = MPSQ_FILES + organism + '/vcf/img/complete/'
    PATH_IMG_VCF_NR = MPSQ_FILES + organism + '/vcf/img/nr/'
    PATH_IMG_VCF_P = MPSQ_FILES + organism + '/vcf/img/partial/'

    gff_name1 = find_multiple_file(sample+'[._-]?[!0-9]*', PATH_IMG_GFF_C) 
    gff_name2 = find_file(sample+'[._-]?[!0-9]*', PATH_IMG_GFF_NR)
    gff_name3 = find_multiple_file(sample+'[._-]?[!0-9]*', PATH_IMG_GFF_P)

    # We use the find_file function here and not the find_path because we need a relative path and not an absolute one 

    if gff_name1:
        context['struct_C_img']=[]
        for name in gff_name1 :
            file_path = organism+'/gff/img/complete/' + name
            context['struct_C_img'].append(file_path) 
    
    if gff_name2 != None :
        file_path = organism+'/gff/img/nr/' + gff_name2
        context['struct_NR_img'] = file_path
    
    if gff_name3 != None :
        context['struct_P_img']=[]
        for name in gff_name3 :
            file_path = organism+'/gff/img/partial/' + name
            context['struct_P_img'].append(file_path)

    vcf_name1 = find_file(sample+'[._-]?[!0-9]*', PATH_IMG_VCF_C)
    vcf_name2 = find_file(sample+'[._-]?[!0-9]*', PATH_IMG_VCF_NR)
    vcf_name3 = find_file(sample+'[._-]?[!0-9]*', PATH_IMG_VCF_P)

    if vcf_name1 != None :
        file_path = organism+'/vcf/img/complete/' + vcf_name1
        context['var_img_C'] = file_path
    
    if vcf_name2 != None :
        file_path = organism+'/vcf/img/nr/' + vcf_name2
        context['var_img_NR'] = file_path
    
    if vcf_name3 != None :
        file_path = organism+'/vcf/img/partial/' + vcf_name3
        context['var_img_P'] = file_path


    # To handle the case where there is no visualization available, but the file exists and is downloadable :
    PATH_FILE_GFF_C = MPSQ_FILES + organism + '/gff/files/complete/'
    PATH_FILE_GFF_NR = MPSQ_FILES + organism + '/gff/files/nr/'
    PATH_FILE_GFF_P = MPSQ_FILES + organism + '/gff/files/partial/'
    PATH_FILE_VCF_C = MPSQ_FILES + organism + '/vcf/files/complete/'
    PATH_FILE_VCF_NR = MPSQ_FILES + organism + '/vcf/files/nr/'
    PATH_FILE_VCF_P = MPSQ_FILES + organism + '/vcf/files/partial/'

    gff_filename1 = find_file(sample+'[._-]?[!0-9]*', PATH_FILE_GFF_C) 
    gff_filename2 = find_file(sample+'[._-]?[!0-9]*', PATH_FILE_GFF_NR)
    gff_filename3 = find_file(sample+'[._-]?[!0-9]*', PATH_FILE_GFF_P)
    
    if gff_filename1:
        file_path = organism+'/gff/files/complete/' + gff_filename1
        context['struct_C_file'] = file_path
    
    if gff_filename2 != None :
        file_path = organism+'/gff/files/nr/' + gff_filename2
        context['struct_NR_file'] = file_path
    
    if gff_filename3 != None :
        file_path = organism+'/gff/files/partial/' + gff_filename3
        context['struct_P_file'] = file_path

    vcf_filename1 = find_file(sample+'[._-]?[!0-9]*', PATH_FILE_VCF_C)
    vcf_filename2 = find_file(sample+'[._-]?[!0-9]*', PATH_FILE_VCF_NR)
    vcf_filename3 = find_file(sample+'[._-]?[!0-9]*', PATH_FILE_VCF_P)

    if vcf_filename1 != None :
        file_path = organism+'/vcf/files/complete/' + vcf_filename1
        context['var_file_C'] = file_path
    
    if vcf_filename2 != None :
        file_path = organism+'/vcf/files/nr/' + vcf_filename2
        context['var_file_NR'] = file_path
    
    if vcf_filename3 != None :
        file_path = organism+'/vcf/files/partial/' + vcf_filename3
        context['var_file_P'] = file_path



    # Phylotree
    PATH_PHYLO = MPSQ_FILES + organism + '/phylo/'
    
    #For viruses :
    if organism=='Ostreid herpesvirus-1': # ! hard
        phylo_path = find_path('*BestModel'+'*', PATH_PHYLO)
        if phylo_path != None :
            phylo_path = Path(phylo_path)
            with open(phylo_path, 'r') as f:
                phylotree =  f.read().rstrip('\n')
            f.close()

        phylo_path = find_path('*jmodeltest'+'*', PATH_PHYLO)
        if phylo_path != None :
            phylo_path = Path(phylo_path)
            with open(phylo_path, 'r') as f:
                lines =  f.readlines()
                last = lines[-1]
                first = True
                for line in lines:
                    sep = line.split()
                    if first:
                        jmodel = sep[2]
                        first = False
                    if line.startswith("Arguments"):
                        j_command_full = line[11:]  # 11 corresponds to the index of the first caracter starting the jmodeltest command in the string
                        replaced = re.sub('\/.*\.phy', 'alignment.phy', j_command_full)
                        j_command = re.sub('\/.*\.out', 'jmodeltest.out', replaced)
                    if line.startswith(" Phyml version"):
                        phyml = sep[3]
                    if line.startswith("phyml "):
                        phyml_command = re.sub('\/.*\.phy', 'alignment.phy', line)
                    if line is last :
                        model = sep[1]
            f.close()

        if phylotree is not None:
            context['phylotree'] = phylotree    
        if jmodel is not None:
            context['jmodel'] = jmodel
        if j_command is not None:
            context['j_command'] = j_command
        if model is not None:
            context['model'] = model
        if phyml is not None:
            context['phyml'] = phyml
        if phyml_command is not None:
            context['phyml_command'] = phyml_command
    
    else :
        phylo_path = find_path('*andi'+'*', PATH_PHYLO)
        if phylo_path != None:
            phylo_path = Path(phylo_path)
            alg_free_tool="Andi"
            tree_generator=""
            with open(phylo_path, 'r') as f:
                lines =  f.readlines()
                for line in lines:
                    sep = line.split(" ")
                    if "andi " in line:
                        if "command" not in line:
                            alg_free_version=sep[1]

                        else:
                            alg_free_command=line[len("andi_command :"):]
                    elif "version : " in line:
                        if tree_generator == "":
                            tree_generator=sep[1][1:]
                            tree_generator_version=sep[-1][:-2]
                        else:
                            tree_generator=tree_generator+" & "+sep[1][1:]
                            tree_generator_version=tree_generator_version+" & "+sep[-1][:-2]
            f.close()
            if alg_free_tool is not None:
                context['alg_free_tool'] = alg_free_tool
            if alg_free_version is not None:
                context['alg_free_version'] = alg_free_version
            if tree_generator is not None:
                context['tree_generator'] = tree_generator
            if tree_generator_version is not None:
                context['tree_generator_version'] = tree_generator_version
            if alg_free_command is not None:
                context['alg_free_command'] = alg_free_command
    
    return render(request, 'info_pat.html', context)

def download_info_pat(request, organism, sample, listDl):
    if listDl != 'zzzz': 

        # Setup the different paths needed
        PATH_FASTA_C = MPSQ_FILES + organism + '/fasta/complete/'
        PATH_FASTA_NR = MPSQ_FILES + organism + '/fasta/nr/'
        PATH_FASTA_P = MPSQ_FILES + organism + '/fasta/partial/'
        PATH_GFF_C = MPSQ_FILES + organism + '/gff/files/complete/'
        PATH_GFF_NR = MPSQ_FILES + organism + '/gff/files/nr/'
        PATH_GFF_P = MPSQ_FILES + organism + '/gff/files/partial/'
        PATH_VCF_C = MPSQ_FILES + organism + '/vcf/files/complete/'
        PATH_VCF_NR = MPSQ_FILES + organism + '/vcf/files/nr/'
        PATH_VCF_P = MPSQ_FILES + organism + '/vcf/files/partial/'

        # Set up ZIP folder
        zip_filename = "data_folder.zip"
        byte_stream = io.BytesIO()
        zf = zipfile.ZipFile(byte_stream, "w") 

        if "A" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_FASTA_C)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the ZIP folder
            zf.write(filepath, "genome/"+sample+'.fasta')   
        
        if "B" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_FASTA_NR)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "genome/"+sample+'_NR.fasta')   

        if "C" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_FASTA_P)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "genome/"+sample+'_P.fasta')   

        if "D" in listDl:
            # We open a CSV variable
            metadata_csv = io.StringIO()
            writer = csv.writer(metadata_csv)
            writer.writerow(['sample_name', 'genbank_id', 'strain', 'isolate', 'host', 'collection_year', 'collection_date', 'country_sample', 'geo_loc_name', 'latitude', 'longitude', 'nature_coordinates', 'host_stage', 'isolation_source', 'pool_individual', 'sample_conservation_method', 'sequencing_technology', 'sequencer', 'additional_info_sequencing', 'nb_reads', 'seq_coverage', 'seq_size', 'viral_charge', 'assembly', 'genome_type', 'structure', 'organism', 'doi', 'notes'])
            
            # We get the values of the sample and write them into the CSV
            queryset = Pathogen.objects.filter(organism=organism, sample_name=sample).values_list('sample_name', 'genbank_id', 'strain', 'isolate', 'host', 'collection_year', 'collection_date', 'country_sample', 'geo_loc_name', 'latitude', 'longitude', 'nature_coordinates', 'host_stage', 'isolation_source', 'pool_individual', 'sample_conservation_method', 'sequencing_technology', 'sequencer', 'additional_info_sequencing', 'nb_reads', 'seq_coverage', 'seq_size', 'viral_charge', 'assembly', 'genome_type', 'structure', 'organism', 'doi', 'notes')
            for value in queryset: 
                writer.writerow(value) # queryset has a list of object with values inside, so we write the whole line in one go
            # We set the cursor at the beginning of the file
            metadata_csv.seek(0)
            # Push the CSV into the ZIP
            zf.writestr('metadata.csv', metadata_csv.read())   

        if "E" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_GFF_C)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "gff/"+sample+'.gff')   

        if "F" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_GFF_NR)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "gff/"+sample+'_NR.gff')  

        if "G" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_GFF_P)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "gff/"+sample+'_P.gff')   
        
        if "H" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_VCF_C)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "vcf/"+sample+'.vcf')   
        
        if "I" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_VCF_NR)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "vcf/"+sample+'_NR.vcf')   
        
        if "J" in listDl:
            filepath = find_path(sample+'[._-]?[!0-9]*', PATH_VCF_P)
            # Open the file for reading content
            path = open(filepath, 'rb')
            # Set the mime type
            mime_type, _ = mimetypes.guess_type(filepath)
            response = HttpResponse(path, content_type=mime_type)
            # Set the HTTP header for sending normally to browser but here into the ZIP file
            response['Content-Disposition'] = "attachment; sampleName=%s" % sample

            # Write the file to the zip folder
            zf.write(filepath, "vcf/"+sample+'_P.vcf')   

        # Close and send the zip to the user
        zf.close()
        response = HttpResponse(byte_stream.getvalue(), content_type="application/x-zip-compressed")
        response['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
        return response        

    else:
       return info_pat(request, organism=organism, sample=sample)
