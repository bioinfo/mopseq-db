#!/usr/bin/env bash

#Get environment variable to access jModeltest folder
$VIRTUAL_ENV = echo $VIRTUAL_ENV | sed "s#\(.*\)/\$#\1#" #Delete slash at the end if here

#Get paths arguments 
path_files=("$1"*.f*)
path_phylo="$2"
jpath="$3"

#Put sequences together in a multi-fasta file for MAFFT
rm "$path_phylo"* 2>/dev/null #To remove error message if file doesnt already exists
for f in "${path_files[@]}"
do
    echo \>$(basename "${f::-6}") >> "${path_phylo}seq_conc.fasta" #We put only the sequences names as headers in the multifasta file to have a tree with clean names
    tail -n +2 "${f}" >> "${path_phylo}seq_conc.fasta"
done

#Alignment
mafft --thread -1 --auto --inputorder "${path_phylo}seq_conc.fasta" > "${path_phylo}alignment.fasta"  #-1 let the command automaticaly choose number of thread to use

#Since its local deployment, get number of thread minus two to use 
threads=$(lscpu -p | grep -c "^[0-9]")
nb_thread=$(($threads-2))

#Convert alignment to PHYLIP format
java -jar "$jpath" -tr $nb_thread -d "${path_phylo}alignment.fasta" -getPhylip

#Compute best model for alignment
echo "Jmodeltest computing best model..."
java -jar "$jpath" -tr $nb_thread -d "${path_phylo}alignment.fasta.phy" -s 3 -i -g 8 -f -AIC -o "${path_phylo}jmodeltest.out"

#Get command for PhyML from jmodeltest to get the command arguments; then execute it.
command_jmodel=$(grep "phyml " "${path_phylo}jmodeltest.out") #grep is case and espace sensitive

command_arg=$(echo "$command_jmodel" | sed -e 's/^.*tmp\/[a-zA-Z0-9]*\.phy//g' )

phyml-mpi -i "${path_phylo}alignment.fasta.phy" $command_arg #Execute the PhyML command

#Delete files not wanted and add ".tree" extension to tree file
rm "$path_phylo"*stats*
mv "$path_phylo"*tree* "$path_phylo"BestModel.tree
rm "$path_phylo"*fasta.phy
rm "$path_phylo"seq_conc.fasta

