#!/usr/bin/env Rscript

#install.packages("ape") #a installer dans l'env conda au préalable
library(ape)

#Recovery of metadata
paste("R version", getRversion(), sep=" : ")
paste("ape library version", packageVersion("ape"), sep=" : ")

args <- commandArgs(TRUE)
file <- args[1]
mat<-as.matrix(read.table(file, sep=" ", skip=1))
names<-mat[,1]
mat<-mat[,-c(1)]
colnames(mat)<-names
rownames(mat)<-names

m <- mapply(mat, FUN=as.numeric)
mat2 <- matrix(data=m, ncol=dim(mat)[1], nrow=dim(mat)[2])
colnames(mat2)<-names
rownames(mat2)<-names

dist_mat<-as.dist(mat2)
nj_tree <- ape::nj(dist_mat)

ape::write.tree(nj_tree, file=paste('BestModel.tree', sep=""))
