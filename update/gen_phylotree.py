#########################################################################################
# This script generates the phylogenetic tree of pathogens 
# Author: Jean-Christophe Mouren
# Date: 2022-07-18
# Usage: To be called with MPSQ_upd.py
#########################################################################################

import os, re, subprocess

def gen_phylotree(**kwargs):

    liste_pat = kwargs.get('liste_pat')

    queryset = kwargs.get('queryset') 

    missing_values = ["NA","TBD","Unknown"]
    
    if len(liste_pat) > 1 :
        while (True):
            choix = input("""You are about to launch several jobs to redo the genetic tree files of several pathogens. Are you sure you want to proceed ? [yes/no]\n""")
            if choix == "yes":
                break
            elif choix == "no":
                print("command aborted.")
                return
            else:
                continue 
    
    # Setup of path variables for verification
    MPSQ_FILES = os.getenv('MPSQ_FILES')
    if MPSQ_FILES[-1] != "/": # We verify that the end slash is present
        MPSQ_FILES += "/"
    
    VIRTUAL_ENV = os.getenv('VIRTUAL_ENV')
    if VIRTUAL_ENV[-1] == "/":
        jpath = VIRTUAL_ENV+"jmodeltest-2.1.10/jModelTest.jar"
    else:
        jpath = VIRTUAL_ENV+"/jmodeltest-2.1.10/jModelTest.jar"
    
    if os.path.exists(jpath) == False:
        print("Can't find the jModeltest-2.1.10 directory in the virtual env directory (VIRTUAL_ENV). Please refers to README.md.\n")
        return

    MPSQ_PROD = os.getenv('MPSQ_PROD') # To know which script to launch (pbs on DATARMOR or local job)

    for pathogen in liste_pat:

        if pathogen == "ALL":
                    continue

        PATH_PHYLO = MPSQ_FILES + pathogen + '/phylo/'
    
        PATH_FASTA_C = MPSQ_FILES + pathogen + '/fasta/complete/'
        PATH_FASTA_NR = MPSQ_FILES + pathogen + '/fasta/nr/'
        PATH_FASTA_P = MPSQ_FILES + pathogen + '/fasta/partial/'
        counter_C = 0
        counter_NR = 0
        counter_P = 0

        for files in os.listdir(PATH_FASTA_C):
            counter_C += 1

            name = str(files)
            if  name[-6] != "." or name[-5] != "f" or name[-4] != "a" or name[-3] != "s" or name[-2] != "t" or name[-1] != "a" :
                print("The folder seems to have files with incorrect extension:",PATH_FASTA_C+name,"Please run --checkup before re-attempting.")
                return
            
            found = False
            for sample in queryset:
                if sample.organism == pathogen and re.match(sample.sample_name , name) and ("Complete" in sample.genome_type or not sample.genome_type or sample.genome_type in missing_values):
                    found = True
                    break
     
            if found == False:
                print("Sequence not referenced in the database or wrongly distributed:", PATH_FASTA_C+name, "Please update the database (--metadata) and do a --checkup.")
                return


        for files in os.listdir(PATH_FASTA_NR):
            counter_NR += 1

            name = str(files)
            if  name[-6] != "." or name[-5] != "f" or name[-4] != "a" or name[-3] != "s" or name[-2] != "t" or name[-1] != "a" :
                print("The folder seems to have files with incorrect extension:",PATH_FASTA_NR+name,"Please run --checkup before re-attempting.")
                return
            
            found = False
            for sample in queryset:
                if sample.organism == pathogen and re.match(sample.sample_name , name) and "Non-redundant" in sample.genome_type:
                    found = True
                    break
     
            if found == False:
                print("Sequence not referenced in the database or wrongly distributed:", PATH_FASTA_NR+name, "Please update the database (--metadata) and do a --checkup.")
                return

        for files in os.listdir(PATH_FASTA_P):
            counter_P += 1

            name = str(files)
            if  name[-6] != "." or name[-5] != "f" or name[-4] != "a" or name[-3] != "s" or name[-2] != "t" or name[-1] != "a" :
                print("The folder seems to have files with incorrect extension:",PATH_FASTA_P+name,"Please run --checkup before re-attempting.")
                return

            found = False
            for sample in queryset:
                if sample.organism == pathogen and re.match(sample.sample_name , name) and "Partial" in sample.genome_type:
                    found = True
                    break
     
            if found == False:
                print("Sequence not referenced in the database or wrongly distributed:", PATH_FASTA_P+name, "Please update the database (--metadata) and do a --checkup.")
                return

        if counter_NR > 1:
            choix_dir = PATH_FASTA_NR
            counter=counter_NR
        elif counter_C > 1:
            choix_dir = PATH_FASTA_C
            counter=counter_C
        elif counter_P > 1:
            choix_dir = PATH_FASTA_P
            counter=counter_P
        else:
            print(pathogen, "does not have enough sequences files. Continuing with next pathogen.")
            continue
        
        if choix_dir == PATH_FASTA_NR:
            try:
                if  ((counter_C*100)/counter_NR)-100 > 20:
                    while(True):
                        choix= input("There are 20%/ more sequences in the complete sequences directory instead of the non-redundant directory. Use sequences with complete genome instead of non-redundant ? [yes/no]") 
                        if choix == "yes":
                            choix_dir = PATH_FASTA_C
                            counter=counter_C
                            break
                        elif choix == "no":
                            break
                        else:
                            continue
            except ZeroDivisionError:
                pass

            try:
                if  ((counter_P*100)/counter_NR)-100 > 20:
                    while(True):
                        choix= input("There are 20%/ more sequences in the partial sequences directory instead of the non-redundant directory. Use sequences with partial genome instead of non-redundant ? [yes/no]") 
                        if choix == "yes":
                            choix_dir = PATH_FASTA_P
                            ounter=counter_P
                            break
                        elif choix == "no":
                            break
                        else:
                            continue
            except ZeroDivisionError:
                pass

        path_script = os.path.dirname(os.path.abspath(__file__))
        
        #Phylo is not generated with the same tools for Viruses and Bacteria/Protozoa 
        if queryset.filter(organism=pathogen)[0].kingdom != 'Virus':
            command = "qsub  -v files_path="+choix_dir+",matrice_name="+pathogen+str(counter)+",out_path="+PATH_PHYLO+" "+path_script+"BigGenome_phylo_pipeline.pbs"
            subprocess.run(command, shell=True)
            print(pathogen, "phylotree job submitted.")
        elif str(MPSQ_PROD) == "True":
            command = "qsub -v path_files=\""+choix_dir+"\",path_phylo=\""+PATH_PHYLO+"\"+,jpath=\""+jpath+"\" "+path_script+"/phylotree.pbs"
            subprocess.run(command, shell=True)
            print(pathogen, "phylotree job submitted.")

        elif str(MPSQ_PROD) == "False":
            command = path_script+'/phylotree.sh '+'"'+choix_dir+'" "'+PATH_PHYLO+'" "'+jpath+'"'
            print("Job execution please wait...")
            subprocess.run(command, shell=True)
            print(pathogen, "phylotree job over.\n")

    return
               