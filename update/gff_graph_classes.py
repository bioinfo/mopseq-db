#########################################################################################################
# This script contains different classes to generate struct images based on the pathogen wanted
# Author: Jean-Christophe Mouren, Maude Jacquot
# Creation date: 2022-05-28
# Last modified: 2024-02-21
# Usage: To be called in MPSQ_upd.py
#
# References: http://www.python-simple.com/img/img46.png
#             https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md
#             http://genometools.org/cgi-bin/gff3validator.cgi
#########################################################################################################

from dna_features_viewer import BiopythonTranslator

"""                                                                             !
! Please when creating a new custom class, pay attention to call it the same way the pathogen name is recorded in the database (REPLACE SPACES AND "-" BY AN UNDERSCORE) !
                                                                                    !
Custom translator to customise the structure image theme:
- compute_feature_color determines the color of the box based on the type contained in the gff file (value in the third column).
- compute_feature_label determines which box will have a label displayed, and what is that label.
- compute_feature_html determines the label displayed when the mouse is hovering on an element.
- compute_filtered_features determines which element to display.
"""

class ostreid_herpesvirus_1(BiopythonTranslator):
    def compute_feature_color(self, feature):
        if feature.type == "sequence_feature" or feature.type == "misc_feature" :
            # element identification
            name_qualifier = feature.qualifiers.get('Name')
            note_qualifier = feature.qualifiers.get('Note')
            if name_qualifier is not None and note_qualifier is not None:
                element = name_qualifier[0]
            elif name_qualifier is not None:
                element = name_qualifier[0]
            elif note_qualifier is not None:
                element = note_qualifier[0]
            else:
                element = feature.type

            #element color
            if element in ["IRL", "TRL"]:
                return "green"
            elif element in ["IRS", "TRS"]:
                return "red"
            elif element in ["UL", "US"]:
                return "gray"
            elif element == "X":
                return "fuchsia"
            else:
                return "black"
        elif feature.type == "stem_loop":
            return "yellow"
        elif feature.type == "CDS":
            return "cyan"
        elif feature.type == "gene":
            return "blue"
        elif feature.type == "sequence_alteration":
            return "indigo"
        elif feature.type == "terminator":
            return "orange"
        elif feature.type == "region":
            return "lightpink"
        elif feature.type == "repeat_region":
            return "brown"
        else:
            return "lightgray"

    def compute_feature_label(self, feature):
        if feature.type == "stem_loop":
            return feature.type
        else:
            return None

    def compute_feature_html(self, feature):
        if feature.type == "sequence_feature" or feature.type == "misc_feature":
            id_qualifier = feature.qualifiers.get('ID')
            name_qualifier = feature.qualifiers.get('Name')
            note_qualifier = feature.qualifiers.get('Note')
            if name_qualifier is not None:
                element = name_qualifier[0]
            elif id_qualifier is not None:
                element = id_qualifier[0]
            elif note_qualifier is not None:
                element = note_qualifier[0]
            else:
                element = "Feature"
            return element
        elif feature.type == "stem_loop":
            return "Stem loop"
        elif feature.type in ["CDS", "gene"]:  #We want to get the ORF qualification if possible
            product_qualifier = feature.qualifiers.get('product')
            name_qualifier = feature.qualifiers.get('Name')
            note_qualifier = feature.qualifiers.get('Note')
            if product_qualifier is not None:
                 element = product_qualifier[0]
            elif name_qualifier is not None :
                element = name_qualifier[0]
            elif note_qualifier is not None:
                element = note_qualifier[0]
            else:
                element = "CDS"
            return element

    def compute_filtered_features(self, features):
        return [
            feature for feature in features
            if (feature.type not in ["repeat_region", "terminator", "region","extracted region"] )
        ]

# other pathogens
class Default_Translator(BiopythonTranslator):
    def compute_feature_color(self, feature):
        if feature.type == "CDS":
            return "cyan"
        elif feature.type == "terminator":
            return "green"
        elif feature.type == "region":
            return "dimgray"
        elif feature.type == "sequence_feature" or feature.type == "misc_feature":
            return "silver"
        elif feature.type == "gene":
            return "blue"
        elif feature.type == "sequence_alteration":
            return "red"
        elif feature.type == "repeat_region":
            return "purple"
        elif feature.type == "stem_loop":
            return "yellow"
        else:
            return "gold"

    def compute_feature_label(self, feature):
        if feature.type == 'sequence_feature' or feature.type == "misc_feature":
            try:
                element = feature.qualifiers['ID'][0] #Name=>ID
            except Exception:
                return feature.qualifiers['Note'][0]
            return element
        if  feature.type == "stem_loop":
            return feature.type
        else:
            return None

    def compute_feature_html(self, feature):
        if feature.type == 'sequence_feature' or feature.type == "misc_feature":
            id_qualifier = feature.qualifiers.get('ID')
            name_qualifier = feature.qualifiers.get('Name')
            note_qualifier = feature.qualifiers.get('Note')
            if name_qualifier is not None:
                element = name_qualifier[0]
            elif id_qualifier is not None:
                element = id_qualifier[0]
            elif note_qualifier is not None:
                element = note_qualifier[0]
            else :
                element== "Feature"
            return element
        elif feature.type == "stem_loop":
            try:
                element = feature.qualifiers['Note'][0]
            except Exception:
                return feature.type
            return element
        elif  feature.type == "CDS" or feature.type == "gene":
            try:
                element = feature.qualifiers['ID'][0]
            except Exception:
                return feature.qualifiers['Note'][0]
            return element
        else:
            return feature.type

    def compute_filtered_features(self, features):
        return [
            feature for feature in features
            if (feature.type not in ["region"] )
        ]