#########################################################################################
# This script generates the html files containing the vector images of the vcf 
# Author: Jean-Christophe Mouren
# Date: 2022-06-30
# Usage: To be called with MPSQ_upd.py
#
# References : https://docs.bokeh.org/en/latest/docs/user_guide.html
#              https://brentp.github.io/cyvcf2/
#########################################################################################

from cyvcf2 import VCF, Writer
import numpy as np
import os, re, fnmatch
from bokeh.models import ColumnDataSource, LabelSet, Toggle
from bokeh.plotting import figure, output_file, save
from bokeh.layouts import layout, column
from posixpath import basename


def find_path(pattern, path): #Fonction pour trouver le fichier que l'on lui passe
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                return os.path.join(root, name) #Il retourne le chemin vers le fichier correspondant

def set_AF(fname, file_path):

    file = file_path+fname
    vcf = VCF(file)

    if "AF" not in vcf: 
        vcf.add_info_to_header({'ID': 'AF', 'Description': 'Estimated allele frequency in the range (0,1]','Type':'Float', 'Number': 'A'})
    else:
        compteur = 0 #We check if the AF is already settled correctly (we take the 8 first recorded variants to see)
        af = []
        for variant in vcf:
            #try:
            if "AF" in variant.INFO:
                af.append(variant.INFO["AF"])
            #except Exception:
            else:
                pass
            compteur += 1
            if compteur == 8:
                break

        for i in af:
            if i > 0 and i < 1:
                return
            else:
                break

    w = Writer(file+".cp", vcf)

    for variant in vcf:
        ao = ([])
        #try:
        if "AO" in variant.INFO:
            ao = np.append(ao,variant.INFO["AO"])
        #except KeyError:
        elif "AC" in variant.INFO:
        #    try:
            ao = np.append(ao,variant.INFO["AC"])
            #except KeyError:
        else: 
            w.write_record(variant)
            continue
        ao = ao.tolist()

        dp = ([])
        dp = np.append(dp,variant.INFO["DP"])
        dp = dp.tolist()

        af = []
        for i in range(len(ao)):
            if len(dp) == len(ao) :
                af.append(ao[i]/dp[i])
            else:
                af.append(ao[i]/dp[0])

        if len(af) > 1: 
            variant.INFO["AF"] = ",".join(str(x) for x in af)
        else:
            variant.INFO["AF"] = af[0]
        
        w.write_record(variant)

    w.close() 
    vcf.close()
    os.remove(file)
    os.rename(file+".cp", file)

def gen_graph(fname, path_file, path):
    depth_axe_y = [] # The INFO.["DP"] info of the VCF  <- axe y of the graph, the x axe will be ref_axe_x
    
    ref_inter_bottom = [] # Always 0. Define the quadrilatere baseline on the graph
    ref_inter_top = [] # The INFO.["DP"] info of the VCF concerning the intervariations zone <- axe y of the graph, the x axe will be ref_axe_x
    ref_inter_left_x = [] # The POS info of the VCF concerning the intervariations zone <- axe y of the graph, the x axe will be ref_axe_x
    ref_inter_right_x = []

    ref_label = [] # The REF info of the vcf
    ref_axe_y = [] # 0 for each value 
    ref_axe_x = [] # The POS info of the vcf

    snp_label = ([]) # The ALT info of the vcf depending on INFO.["TYPE"]
    snp_axe_y = ([]) # The allele frequency (INFO.["AF"])
    snp_axe_x = [] # The POS info of the vcf

    ins_label = ([]) # The ALT info of the vcf depending on INFO.["TYPE"]
    ins_axe_y = ([]) # The allele frequency (INFO.["AF"])
    ins_axe_x = [] # The POS info of the vcf

    del_label = ([]) # The ALT info of the vcf depending on INFO.["TYPE"]
    del_axe_y = ([]) # The allele frequency (INFO.["AF"])
    del_axe_x = [] # The POS info of the vcf

    complex_label = ([]) # The ALT info of the vcf depending on INFO.["TYPE"]
    complex_axe_y = ([]) # The allele frequency (INFO.["AF"])
    complex_axe_x = [] # The POS info of the vcf

    for variant in VCF(path_file+fname): # works with VCF('some.bcf')         alt_bon = bool(re.search('<\*>', variant.ALT[0]))  If test vcf file, pay attention to sequence length and end postion of last variant to not be out of range
        alt_bon = bool(re.match('^[A-Za-z]*$', variant.ALT[0]))
        if alt_bon == True:
            # We get the values of depth
            depth_axe_y.append(variant.INFO["DP"])
            #faire propre axe x pour depth car on veut coverage des bases normales sans variant 
            # We get the values for the reference graph
            ref_label.append(variant.REF)
            ref_axe_y.append(0)
            ref_axe_x.append(variant.POS)

        else:
            try:
                ref_inter_right_x.append(variant.INFO["END"]) # If plotting the inter varaitions coverage is not working, maybe this information is missing in the vcf
            except Exception:
                continue

            ref_inter_bottom.append(0)
            ref_inter_top.append(variant.INFO["DP"])
            ref_inter_left_x.append(variant.POS)

            continue

        # We get the values for all alternate graph depending on the type of variation
        alt_type_list = []
        try:
            if "," in variant.INFO["TYPE"]:
                alt_type_list = variant.INFO["TYPE"].split(',')
            else:
                alt_type_list = [variant.INFO["TYPE"]]

            for alt_type in alt_type_list:
                if alt_type == "snp" or alt_type == "mnp":
                    if len(variant.ALT) > 1:
                        for i in variant.ALT:
                            snp_axe_x.append(variant.POS)
                            snp_label = np.append(snp_label, i)
                    else:
                        snp_axe_x.append(variant.POS)
                        snp_label = np.append(snp_label, variant.ALT)
                    snp_axe_y = np.append(snp_axe_y, variant.INFO["AF"])
                
                elif alt_type == "ins":
                    if len(variant.ALT) > 1:
                        for i in variant.ALT:
                            ins_axe_x.append(variant.POS)
                            ins_label = np.append(ins_label, i)
                    else:
                        ins_axe_x.append(variant.POS)
                        ins_label = np.append(ins_label, variant.ALT)
                    ins_axe_y = np.append(ins_axe_y, variant.INFO["AF"])

                elif alt_type == "del":
                    if len(variant.ALT) > 1:
                        for i in variant.ALT:
                            del_axe_x.append(variant.POS)
                            del_label = np.append(del_label, i)
                    else:
                        del_axe_x.append(variant.POS)
                        del_label = np.append(del_label, variant.ALT)
                    del_axe_y = np.append(del_axe_y, variant.INFO["AF"])

                elif alt_type == "complex":
                    if len(variant.ALT) > 1:
                        for i in variant.ALT:
                            complex_axe_x.append(variant.POS)
                            complex_label = np.append(complex_label, i)
                    else:
                        complex_axe_x.append(variant.POS)
                        complex_label = np.append(complex_label, variant.ALT)
                    complex_axe_y = np.append(complex_axe_y, variant.INFO["AF"])

                else:
                    continue
        except KeyError:
            pass   

    #Checking of numbers of labels obtained and filtering on frequency if above 10000   AF=0\.[0-9]*;  1483
    filtered = False
    index_af = 0
    list_af_filter = [0.01,0.011,0.012,0.013,0.014,0.015,0.02,0.05,0.1,0.15,0.20,0.30] #A list of filter to put on the AF
    while((len(ref_label)+len(snp_label)+len(ins_label)+len(del_label)+len(complex_label)) > 10000):
        filtered = True

        snp_label_copy = ([])
        snp_axe_y_copy = ([])
        snp_axe_x_copy = []
        
        for af in range(len(snp_axe_y)):
            if snp_axe_y[af] < list_af_filter[index_af]:
                index_ref = ref_axe_x.index(snp_axe_x[af])
                ref_axe_x.pop(index_ref)
                ref_axe_y.pop(index_ref)
                ref_label.pop(index_ref)
                depth_axe_y.pop(index_ref)
                continue
            else:
                snp_axe_y_copy = np.append(snp_axe_y_copy,snp_axe_y[af])
                snp_label_copy = np.append(snp_label_copy,snp_label[af])
                snp_axe_x_copy.append(snp_axe_x[af])

        snp_label = snp_label_copy
        snp_axe_y = snp_axe_y_copy
        snp_axe_x = snp_axe_x_copy

        ins_label_copy = ([])
        ins_axe_y_copy = ([])
        ins_axe_x_copy = []
        
        for af in range(len(ins_axe_y)):
            if ins_axe_y[af] < list_af_filter[index_af]:
                index_ref = ref_axe_x.index(ins_axe_x[af])
                ref_axe_x.pop(index_ref)
                ref_axe_y.pop(index_ref)
                ref_label.pop(index_ref)
                depth_axe_y.pop(index_ref)
                continue
            else:
                ins_axe_y_copy = np.append(ins_axe_y_copy,ins_axe_y[af])
                ins_label_copy = np.append(ins_label_copy,ins_label[af])
                ins_axe_x_copy.append(ins_axe_x[af])

        ins_label = ins_label_copy
        ins_axe_y = ins_axe_y_copy
        ins_axe_x = ins_axe_x_copy

        del_label_copy = ([])
        del_axe_y_copy = ([])
        del_axe_x_copy = []
        
        for af in range(len(del_axe_y)):
            if del_axe_y[af] < list_af_filter[index_af]:
                index_ref = ref_axe_x.index(del_axe_x[af])
                ref_axe_x.pop(index_ref)
                ref_axe_y.pop(index_ref)
                ref_label.pop(index_ref)
                depth_axe_y.pop(index_ref)
                continue
            else:
                del_axe_y_copy = np.append(del_axe_y_copy,del_axe_y[af])
                del_label_copy = np.append(del_label_copy,del_label[af])
                del_axe_x_copy.append(del_axe_x[af])

        del_label = del_label_copy
        del_axe_y = del_axe_y_copy
        del_axe_x = del_axe_x_copy

        complex_label_copy = ([])
        complex_axe_y_copy = ([])
        complex_axe_x_copy = []
        
        for af in range(len(complex_axe_y)):
            if complex_axe_y[af] < list_af_filter[index_af]:
                index_ref = ref_axe_x.index(complex_axe_x[af])
                ref_axe_x.pop(index_ref)
                ref_axe_y.pop(index_ref)
                ref_label.pop(index_ref)
                depth_axe_y.pop(index_ref)
                continue
            else:
                complex_axe_y_copy = np.append(complex_axe_y_copy,complex_axe_y[af])
                complex_label_copy = np.append(complex_label_copy,complex_label[af])
                complex_axe_x_copy.append(complex_axe_x[af])

        complex_label = complex_label_copy
        complex_axe_y = complex_axe_y_copy
        complex_axe_x = complex_axe_x_copy

        index_af += 1

    # Parameters of plot, files output and check statement
    output_file("var_tmp.html")

    plot_check = []

    TOOLTIPS = [
        ("POS", "$x"),
        ("FREQ", "$y"),
    ]

    if filtered:
        filtre = str(list_af_filter[index_af])
        p = figure(title="Variants filtered on "+filtre+" allele frequency", title_location="above", plot_height=400, plot_width=1000, tools='hover,wheel_zoom,reset,box_zoom,pan,save', tooltips=TOOLTIPS)
    else:
        p = figure(title=None, title_location="above", height=400, width=1000, tools='hover,wheel_zoom,reset,box_zoom,pan,save', tooltips=TOOLTIPS)
    p.xaxis[0].axis_label = 'Position (pb)'
    p.yaxis[0].axis_label = 'Frequency'

    # Plot the reference graph
    source_ref = ColumnDataSource(data=dict(position=ref_axe_x, frequency=ref_axe_y, names=ref_label))
    ref_plot = p.scatter(x='position', y='frequency', fill_color='#000000', line_color='#000000', legend_label="REF", source=source_ref) # Color black,  size=12 to change size figure
    labels_ref = LabelSet(x='position', y='frequency', text='names', x_offset=0, y_offset=-15, text_font_size='8pt', source=source_ref)
    p.add_layout(labels_ref)

    # We set the toggle buttons to display the several graphs
    toggle_ref = Toggle(label="REF", button_type="success", active=True, css_classes=["custom_width1"])
    toggle_ref.js_link('active', ref_plot, 'visible')
    toggle_ref.js_link('active', labels_ref, 'visible')

    plot_check.append(toggle_ref)

    # Plot the SNP graph
    #try:
    if snp_label is not None:
        if not isinstance(snp_label, list):
            snp_label = snp_label.tolist()
        if not isinstance(snp_axe_y, list):
            snp_axe_y = snp_axe_y.tolist()

        # We set the colors of the diverses SNP or MNP
        colors_snp =[]
        for snp in snp_label:
            if snp == "A":
                colors_snp.append("#f00020") # Red
            elif snp == "T":
                colors_snp.append("#0000FF") # Blue
            elif snp == "C":
                colors_snp.append("#008000") # Green
            elif snp == "G":
                colors_snp.append("#ff7f00") # Orange
            else:
                colors_snp.append("#ff00ff") # Magenta

        source_snp = ColumnDataSource(data=dict(position=snp_axe_x, frequency=snp_axe_y, names=snp_label, color=colors_snp))
        snp_plot = p.scatter(x='position', y='frequency', color='color', legend_label="SNP, MNP", source=source_snp)
        labels_snp = LabelSet(x='position', y='frequency', text='names', x_offset=0, y_offset=0, text_color='color', text_font_size='8pt', source=source_snp)
        p.add_layout(labels_snp)

        toggle_snp = Toggle(label="SNP/MNP", button_type="success", active=True, css_classes=["custom_width2"])
        toggle_snp.js_link('active', snp_plot, 'visible')
        toggle_snp.js_link('active', labels_snp, 'visible')

        plot_check.append(toggle_snp)

    #except Exception:
    else:
        pass

    # Plot the INS graph
    #try:
    if ins_label is not None:
        if not isinstance(ins_label,list):
            ins_label = ins_label.tolist()
        if not isinstance(ins_axe_y,list):
            ins_axe_y = ins_axe_y.tolist()
    
        source_ins = ColumnDataSource(data=dict(position=ins_axe_x, frequency=ins_axe_y, names=ins_label))
        ins_plot = p.square(x='position', y='frequency', fill_color='#808080', line_color='#808080', legend_label="INS", source=source_ins) # Color gray
        labels_ins = LabelSet(x='position', y='frequency', text='names', x_offset=0, y_offset=0, text_color='#808080', text_font_size='8pt', source=source_ins)
        p.add_layout(labels_ins)

        toggle_ins = Toggle(label="INS", button_type="success", active=True, css_classes=["custom_width3"])
        toggle_ins.js_link('active', ins_plot, 'visible')
        toggle_ins.js_link('active', labels_ins, 'visible')

        plot_check.append(toggle_ins)

    #except Exception:
    else:
        pass

    # Plot the DEL graph
    #try:
    if del_label is not None:
        if not isinstance(del_label, list):
            del_label = del_label.tolist()
        if not isinstance(del_axe_y, list):
            del_axe_y = del_axe_y.tolist()

        source_del = ColumnDataSource(data=dict(position=del_axe_x, frequency=del_axe_y, names=del_label))
        del_plot = p.diamond_dot(x='position', y='frequency', size=7, fill_color='#808080', line_color='#808080', legend_label="DEL", source=source_del) # Color gray
        labels_del = LabelSet(x='position', y='frequency', text='names', x_offset=0, y_offset=0, text_color='#808080', text_font_size='8pt', source=source_del)
        p.add_layout(labels_del)

        toggle_del = Toggle(label="DEL", button_type="success", active=True, css_classes=["custom_width4"])
        toggle_del.js_link('active', del_plot, 'visible')
        toggle_del.js_link('active', labels_del, 'visible')
        
        plot_check.append(toggle_del)

    #except Exception:
    else:
        pass

    # Plot the complex graph
    #try:
    if complex_label is not None:
        if not isinstance(complex_label, list):
            complex_label = complex_label.tolist()
        if not isinstance(complex_axe_y, list):
            complex_axe_y = complex_axe_y.tolist()

        source_complex = ColumnDataSource(data=dict(position=complex_axe_x, frequency=complex_axe_y, names=complex_label))
        complex_plot = p.triangle_pin(x='position', y='frequency', size=7, fill_color='#808080', line_color='#808080', legend_label="Complex", source=source_complex) # Color gray
        labels_complex = LabelSet(x='position', y='frequency', text='names', x_offset=0, y_offset=0, text_color='#808080', text_font_size='8pt', source=source_complex)
        p.add_layout(labels_complex)
        
        toggle_complex = Toggle(label="Complex", button_type="success", active=True, css_classes=["custom_width5"])
        toggle_complex.js_link('active', complex_plot, 'visible')
        toggle_complex.js_link('active', labels_complex, 'visible')

        plot_check.append(toggle_complex)

    #except Exception:
    else:
        pass

    # We plot the depth in a new graph
    plot_dp_check = []

    TOOLTIPS = [
        ("POS", "$x"),
        ("DEPTH", "$y"),
    ]

    d = figure(height=400, width=1000, tools='hover,wheel_zoom,reset,box_zoom,pan,save', tooltips=TOOLTIPS)
    d.xaxis[0].axis_label = 'Position (pb)'
    d.yaxis[0].axis_label = 'Coverage'
    depth_plot = d.vbar(x=ref_axe_x, top=depth_axe_y, color="#ff0000")

    # We set the toggle buttons to display the several graphs
    toggle_depth = Toggle(label="Variations", button_type="success", active=True, css_classes=["custom_width2"])
    toggle_depth.js_link('active', depth_plot, 'visible')
    
    plot_dp_check.append(toggle_depth)

    # Plot the intervariation graph
    if bool(ref_inter_right_x) != False: #ref_inter_right_x is a list not an array, we cannot check if its empty with len() but with bool then. PS: bool check doesnt work on array.
        inter_plot = d.quad(top=ref_inter_top, bottom=ref_inter_bottom, left=ref_inter_left_x, right=ref_inter_right_x)

        toggle_inter = Toggle(label="Remaining sites", button_type="success", active=True, css_classes=["custom_width4"])
        toggle_inter.js_link('active', inter_plot, 'visible')

        plot_dp_check.append(toggle_inter)

    save(column(layout([p], plot_check), layout([d], plot_dp_check)))

    file_name_cropped = fname[:-4]
    with open("var_tmp.html", "r") as f:
        with open(path+file_name_cropped+"_var.html", "w+") as struct_file:
            for number, line in enumerate(f):
                if number == 43: 
                    line=re.sub('id="?(.*?)">', 'id="'+file_name_cropped+'_var">' , line, flags=re.DOTALL)
                    struct_file.write(line)

                elif number == 44: 
                    line=re.sub('{"?(.*?)":', '{"'+file_name_cropped+'":' , line, 1, flags=re.DOTALL)
                    struct_file.write(line)

                elif number == 45:
                    struct_file.write(line)
                    break

        struct_file.close()
    f.close()

def gen_vcf_graph(**kwargs):

    liste_pat = kwargs.get('liste_pat')
    
    queryset = kwargs.get('queryset') 

    overwrite = kwargs.get('overwrite')
    if overwrite == True :
        option = "2"
    else:
        option = "1"

    compteur_duplicate = 0
    compteur_added = 0

    missing_values = ["NA","TBD","Unknown"]

    # Setup of path variables for verification
    MPSQ_FILES = os.getenv('MPSQ_FILES')
    if MPSQ_FILES[-1] != "/": # We verify that the end slash is present
        MPSQ_FILES += "/"

    for pathogen in liste_pat:
        
        if pathogen == "ALL":
            continue

        PATH_VCF_C = MPSQ_FILES + pathogen + '/vcf/files/complete/'
        PATH_VCF_NR = MPSQ_FILES + pathogen + '/vcf/files/nr/'
        PATH_VCF_P = MPSQ_FILES + pathogen + '/vcf/files/partial/'

        PATH_IMG_VCF_C = MPSQ_FILES + pathogen + '/vcf/img/complete/'
        PATH_IMG_VCF_NR = MPSQ_FILES + pathogen + '/vcf/img/nr/'
        PATH_IMG_VCF_P = MPSQ_FILES + pathogen + '/vcf/img/partial/'

        # Verification of the files name and changement if not correct
        
        for sample in queryset:
            if sample.organism == pathogen:
                if sample.genome_type=="Complete and Non-redundant" :
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_VCF_NR)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_NR.vcf":
                            os.rename(filepath, PATH_VCF_NR+sample.sample_name+"_NR.vcf")
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_VCF_C)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_C.vcf":
                            os.rename(filepath, PATH_VCF_C+sample.sample_name+"_C.vcf")
                elif sample.genome_type=="Complete" :
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_VCF_C)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_C.vcf":
                            os.rename(filepath, PATH_VCF_C+sample.sample_name+"_C.vcf")
                elif sample.genome_type=="Non-redundant" :
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_VCF_NR)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_NR.vcf":
                            os.rename(filepath, PATH_VCF_NR+sample.sample_name+"_NR.vcf")
                elif sample.genome_type=="Partial" :
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_VCF_P)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_P.vcf":
                            os.rename(filepath, PATH_VCF_P+sample.sample_name+"_P.vcf")
                else: "Cannot check avalaible vcf files for "+sample.sample_name+" genome as no or uncorrect genome_type provided. Please update information in the database."

        for file in os.listdir(PATH_VCF_C):
            file_name = os.fsdecode(file) # The name of the file is encoded by the system 
            file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample 
            print("Generation of the vcf image for genome "+ file_name_cropped +".")

            if option == "1":
                test_duplicate = find_path(file_name_cropped+'[._-]?[!0-9]*', PATH_IMG_VCF_C)
                if test_duplicate != None:
                    compteur_duplicate += 1
                    continue
            
            set_AF(file_name,PATH_VCF_C) #We call the function to be sure the allele frequency is correctly recorded.
            gen_graph(file_name,PATH_VCF_C,PATH_IMG_VCF_C)
            compteur_added += 1
        
        for file in os.listdir(PATH_VCF_NR):
            file_name = os.fsdecode(file) # The name of the file is encoded by the system 
            file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample 
            print("Generation of the vcf image for genome "+ file_name_cropped +".")

            if option == "1":
                test_duplicate = find_path(file_name_cropped+'[._-]?[!0-9]*', PATH_IMG_VCF_NR)
                if test_duplicate != None:
                    compteur_duplicate += 1
                    continue

            set_AF(file_name,PATH_VCF_NR)
            gen_graph(file_name,PATH_VCF_NR,PATH_IMG_VCF_NR)
            compteur_added += 1
        
        for file in os.listdir(PATH_VCF_P):
            file_name = os.fsdecode(file) # The name of the file is encoded by the system 
            file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample 
            print("Generation of the vcf image for genome "+ file_name_cropped +".")

            if option == "1":
                test_duplicate = find_path(file_name_cropped+'[._-]?[!0-9]*', PATH_IMG_VCF_P)
                if test_duplicate != None:
                    compteur_duplicate += 1
                    continue

            set_AF(file_name,PATH_VCF_P)
            gen_graph(file_name,PATH_VCF_P,PATH_IMG_VCF_P)
            compteur_added += 1
    
        print("\nPathogen:", pathogen, "|" , compteur_added, "html of variations images added,", compteur_duplicate, "duplicates found.")
        compteur_added = 0
        compteur_duplicate = 0

    if(os.path.isfile("var_tmp.html")):
        os.remove("var_tmp.html") 

    return