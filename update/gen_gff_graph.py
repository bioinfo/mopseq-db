#########################################################################################
# This script generates the html files containing the vector images of the gff 
# Author: Jean-Christophe Mouren, Maude Jacquot
# Creation Date: 2022-05-28
# Last modified: 2024-02-20
# Usage: To be called in Update_MOPSEQ.py
#
# References : https://github.com/Edinburgh-Genome-Foundry/DnaFeaturesViewer
#              https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md
#              http://genometools.org/cgi-bin/gff3validator.cgi
#########################################################################################

import update.gff_graph_classes
import os, fnmatch, re
from bokeh.resources import CDN
from bokeh.embed import file_html
from posixpath import basename
from BCBio import GFF
import colorama
from colorama import Fore
from colorama import Style


def find_path(pattern, path): #Fonction pour trouver le fichier que l'on lui passe
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                return os.path.join(root, name) #Il retourne le chemin vers le fichier correspondant


def gen_gff_graph(**kwargs):
    liste_pat = kwargs.get('liste_pat')
    queryset = kwargs.get('queryset') 
    overwrite = kwargs.get('overwrite')
    if overwrite == True :
        option = "2"
    else:
        option = "1"
    
    compteur_duplicate = 0    
    compteur_added = 0
    missing_values = ["NA","TBD","Unknown"]

    # Setup of path variables for verification
    MPSQ_FILES = os.getenv('MPSQ_FILES')
    if MPSQ_FILES[-1] != "/": # We verify that the end slash is present
        MPSQ_FILES += "/"

    for pathogen in liste_pat:
        
        if pathogen == "ALL":
            continue

        PATH_GFF_C = MPSQ_FILES + pathogen + '/gff/files/complete/'
        PATH_GFF_NR = MPSQ_FILES + pathogen + '/gff/files/nr/'
        PATH_GFF_P = MPSQ_FILES + pathogen + '/gff/files/partial/'

        PATH_IMG_GFF_C = MPSQ_FILES + pathogen + '/gff/img/complete/'
        PATH_IMG_GFF_NR = MPSQ_FILES + pathogen + '/gff/img/nr/'
        PATH_IMG_GFF_P = MPSQ_FILES + pathogen + '/gff/img/partial/'

        # Verification of the files name and changement if not correct
        for sample in queryset:
            if sample.organism == pathogen:
                if sample.genome_type == "Complete and Non-redundant" :
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_GFF_NR)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_NR.gff":
                            os.rename(filepath, PATH_GFF_NR+sample.sample_name+"_NR.gff")
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_GFF_C)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_C.gff":
                            os.rename(filepath, PATH_GFF_C+sample.sample_name+"_C.gff")
                elif sample.genome_type=="Complete":
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_GFF_C)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_C.gff":
                            os.rename(filepath, PATH_GFF_C+sample.sample_name+"_C.gff")
                elif sample.genome_type=="Non-redundant" :
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_GFF_NR)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_NR.gff":
                            os.rename(filepath, PATH_GFF_NR+sample.sample_name+"_NR.gff")
                elif sample.genome_type=="Partial":
                    filepath = find_path(sample.sample_name+'[._-]?[!0-9]*', PATH_GFF_P)
                    if filepath != None:
                        if basename(filepath) != sample.sample_name+"_P.gff":
                            os.rename(filepath, PATH_GFF_P+sample.sample_name+"_P.gff")
                else: "Cannot check avalaible gff files for "+sample.sample_name+" genome as no or uncorrect genome_type provided. Please update information in the database."


        # We get the list of classes by alphabetical order currently defined in the class file
        classes = [x for x in dir(update.gff_graph_classes) if isinstance(getattr(update.gff_graph_classes, x), type)]

        # We convert the pathogen name to the same format (no special character except underscore) of the classes name (used to build the graphs) in the file gff_graph_classes.py
        name_pathogen = pathogen.lower()   
        name_pathogen = name_pathogen.replace(' ', '_')
        name_pathogen = name_pathogen.replace('-', '_')
        name_pathogen = name_pathogen.replace('.', '_')
        #print("Converted pathogen name:"+name_pathogen)

        # We check if a custom gff model exists for this pathogen, otherwise we pick the default one 
        modele = ""
        for i in range(len(classes)):
            if name_pathogen == classes[i].lower():
                modele = classes[i]
        if modele == "":
            modele = "Default_Translator"
        #print("Chosen model: "+modele)

        for file in os.listdir(PATH_GFF_C):
            file_name = os.fsdecode(file) # The name of the file is encoded by the system          
            file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample 
            print("Generation of the gff image for genome "+ file_name_cropped +".")

            if option == "1":                
                test_duplicate = find_path(file_name_cropped+'[._-]?[!0-9]*', PATH_IMG_GFF_C)
                if test_duplicate != None:
                    compteur_duplicate += 1
                    continue           
            
            record = getattr(update.gff_graph_classes,modele)().translate_record(record=PATH_GFF_C+file_name)
                
            if record:    
                plot = record.plot_with_bokeh(figure_width=10)
                record.plot_with_bokeh
                with open("struct_tmp.html", "w+") as f:
                    f.write(file_html(plot,CDN))
                    f.seek(0)
                    with open(PATH_IMG_GFF_C+file_name_cropped+"_str.html", "w+") as struct_file:
                        for number, line in enumerate(f):
                            if number == 42: #22 on Bokeh 3.3.4 ! hard
                                line=re.sub('id="?(.*?)">', 'id="'+file_name_cropped+'_str">' , line, flags=re.DOTALL) # We change the automatically generated id by our own corresponding to the ones in the info_pat files
                                struct_file.write(line)

                            elif number == 43: #23 on Bokeh 3.3.4 but not the same line content + ! hard
                                line=re.sub('{"?(.*?)":', '{"'+file_name_cropped+'":' , line, 1, flags=re.DOTALL)
                                struct_file.write(line)

                            elif number == 44: #24 on Bokeh 3.3.4 ! hard
                                struct_file.write(line)
                                break
                    struct_file.close()
                f.close() 
                compteur_added += 1
        
        for file in os.listdir(PATH_GFF_NR):
            file_name = os.fsdecode(file) # The name of the file is encoded by the system 
            file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample
            print("Generation of the gff image for genome "+ file_name_cropped +".")

            if option == "1":                 
                test_duplicate = find_path(file_name_cropped+'[._-]?[!0-9]*', PATH_IMG_GFF_NR)
                if test_duplicate != None:
                    compteur_duplicate += 1
                    continue         

            # We call the function with class chosen
            record = getattr(update.gff_graph_classes,modele)().translate_record(record=PATH_GFF_NR+file_name)

            if record:
                plot = record.plot_with_bokeh(figure_width=10)
                record.plot_with_bokeh

                file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample 
            
                with open("struct_tmp.html", "w+") as f:
                    f.write(file_html(plot,CDN))
                    f.seek(0)
                    with open(PATH_IMG_GFF_NR+file_name_cropped+"_str.html", "w+") as struct_file:
                        for number, line in enumerate(f):
                            if number == 42: # 22 on Bokeh 3.3.4 ! hard
                                line=re.sub('id="?(.*?)">', 'id="'+file_name_cropped+'_str">' , line, flags=re.DOTALL)
                                struct_file.write(line)

                            elif number == 43: # 23 on Bokeh 3.3.4 but not the same line content + ! hard 
                                line=re.sub('{"?(.*?)":', '{"'+file_name_cropped+'":' , line, 1, flags=re.DOTALL)
                                struct_file.write(line)

                            elif number == 44: # 24 on Bokeh 3.3.4 ! hard
                                struct_file.write(line)
                                break

                    struct_file.close()
                #f.close() #useless with "with open"
                compteur_added += 1
            else: 
                print("GFF image not generated, check the gff file")

        for file in os.listdir(PATH_GFF_P):
            file_name = os.fsdecode(file) # The name of the file is encoded by the system 
            file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample
            print("Generation of the gff image for genome "+ file_name_cropped +".")

            if option == "1":
                test_duplicate = find_path(file_name_cropped+'[._-]?[!0-9]*', PATH_IMG_GFF_P)
                if test_duplicate != None:
                    compteur_duplicate += 1
                    continue

            # We call the function with class chosen
            record = getattr(update.gff_graph_classes,modele)().translate_record(record=PATH_GFF_P+file_name)

            plot = record.plot_with_bokeh(figure_width=10)
            record.plot_with_bokeh

            file_name_cropped = file_name[:-4] # We remove the extension to get only the name of the sample 

            with open("struct_tmp.html", "w+") as f:
                f.write(file_html(plot,CDN))
                f.seek(0)
                with open(PATH_IMG_GFF_P+file_name_cropped+"_str.html", "w+") as struct_file:
                    for number, line in enumerate(f):
                        if number == 22: #42 ! hard
                            line=re.sub('id="?(.*?)">', 'id="'+file_name_cropped+'_str">' , line, flags=re.DOTALL)
                            struct_file.write(line)

                        elif number ==23: #43 ! hard 
                            line=re.sub('{"?(.*?)":', '{"'+file_name_cropped+'":' , line, 1, flags=re.DOTALL)
                            struct_file.write(line)

                        elif number == 24: #44 ! hard
                            struct_file.write(line)
                            break

                struct_file.close()
            f.close() 
            compteur_added += 1
        
        print("\nPathogen:", pathogen, "|" , compteur_added, "html of structures images added,", compteur_duplicate, "duplicates found.")
        compteur_added = 0
        compteur_duplicate = 0

    if(os.path.isfile("struct_tmp.html")):
        os.remove("struct_tmp.html") 

    return
