######################################################################################
# This script check the database to see if files are correctly distributed
# Author: Jean-Christophe Mouren
# Date: 2022-06-30
# Usage: To be called in Update_MOPSEQ.py
######################################################################################

import os, shutil, fnmatch
from posixpath import basename
from features.models import Pathogen

def find_duplicate(file, pattern, path): #Fonction pour trouver les fichiers en double e
    for root, dirs, files in os.walk(path):
        dup_list = []
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Traite dossier par dossier
                dup_list.append(os.path.join(root, name)) #Il retourne le chemin vers le fichier correspondant
        if len(dup_list) > 1: # If duplicate found
            str_dup = " | ".join(dup_list)
            file.write("\nDuplicate found : "+str_dup+"... deleting newest occurence.")
            latest_file = max(dup_list, key=os.path.getctime)
            os.remove(latest_file)
    return

def find_path(pattern, path): #Fonction pour trouver le fichier que l'on lui passe
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): #Il match le fichier en regex
                return os.path.join(root, name) #Il retourne le chemin vers le fichier correspondant

def check_up(**kwargs):
    queryset = kwargs.get('queryset') 

    missing_values = ["NA","TBD","Unknown"]

    f = open("checkup.log", "w")

    # Setup of path variable for verification
    MPSQ_FILES = os.getenv('MPSQ_FILES')
    if MPSQ_FILES[-1] != "/": # We verify that the end slash is present
        MPSQ_FILES += "/"

    # First remove pathogen from database when they dont have directories in /data_files/
    print("\n") 
    dic = Pathogen.objects.order_by().values('organism').distinct()
    liste_pat = []
    try:
        for i in dic:
            values = i.values()
            values_iterator = iter(values)
            organism_value = next(values_iterator)
            if os.path.isdir(MPSQ_FILES+organism_value) is False:
                print(organism_value,'directory not found in data_files repository. Deleting pathogen from database...')
                f.write(organism_value+' directory not found in data_files repository. Deleting pathogen from database.\n')
                Pathogen.objects.filter(organism=organism_value).delete()
            else:
                liste_pat.append(organism_value)
    except Exception:
        pass
   
    print("\nPathogens repositories existence checked.\n")

    loop = True
    while(loop):
        check_change = False
        for root, subdirectories, files in os.walk(MPSQ_FILES):
            for file in files:
                if basename((os.path.dirname(root)))=="fasta":
                    if ".gff" in file :
                        check_change = True
                        shutil.move(os.path.join(root,file), os.path.dirname(os.path.dirname(root))+"/gff/files/complete/")
                    elif ".vcf" in file :
                        check_change = True
                        shutil.move(os.path.join(root,file), os.path.dirname(os.path.dirname(root))+"/vcf/files/complete/")
                    elif ("_NR." in file or "non-redundant" in file.lower() or "non_redundant" in file.lower() or "nonredundant" in file.lower()) and ".fasta" in file and basename(root) != "nr":
                        shutil.move(os.path.join(root,file), os.path.dirname(root)+"/nr/")
                    elif ("_P." in file or "partial" in file.lower()) and ".fasta" in file and basename(root) != "partial":
                        shutil.move(os.path.join(root,file), os.path.dirname(root)+"/partial/")
                    elif ".fasta" in file or ".fa" in file or ".fna" in file or ".ffn" in file or ".faa" in file or ".frn" in file :
                        pass
                    else:
                        f.write("\n Incorrect file extension in"+os.path.join(root,file)+"... deleting.")
                        os.remove(os.path.join(root,file))

                if basename(os.path.dirname(os.path.dirname(root)))=="gff" and basename(os.path.dirname(root))=="files":
                    if ".fasta" in file or ".fa" in file or ".fna" in file or ".ffn" in file or ".faa" in file or ".frn" in file :
                        check_change = True
                        shutil.move(os.path.join(root,file), os.path.dirname(os.path.dirname(root))+"/fasta/complete/")
                    elif ".vcf" in file :
                        check_change = True
                        shutil.move(os.path.join(root,file), os.path.dirname(os.path.dirname(root))+"/vcf/files/complete/")
                    elif ("_NR." in file or "non-redundant" in file.lower() or "non_redundant" in file.lower() or "nonredundant" in file.lower()) and ".gff" in file and basename(root) != "nr":
                        shutil.move(os.path.join(root,file), os.path.dirname(root)+"/nr/")
                    elif ("_P." in file or "partial" in file.lower()) and ".gff" in file and basename(root) != "partial":
                        shutil.move(os.path.join(root,file), os.path.dirname(root)+"/partial/")
                    elif ".gff" in file :
                        pass
                    else:
                        f.write("\n Incorrect file extension in "+os.path.join(root,file)+"... deleting.")
                        os.remove(os.path.join(root,file))

                if basename(os.path.dirname(os.path.dirname(root)))=="vcf" and basename(os.path.dirname(root))=="files":
                    if ".fasta" in file or ".fa" in file or ".fna" in file or ".ffn" in file or ".faa" in file or ".frn" in file :
                        check_change = True
                        shutil.move(os.path.join(root,file), os.path.dirname(os.path.dirname(root))+"/fasta/complete/")
                    elif ".gff" in file :
                        check_change = True
                        shutil.move(os.path.join(root,file), os.path.dirname(os.path.dirname(root))+"/gff/files/complete/")
                    elif ("_NR." in file or "non-redundant" in file.lower() or "non_redundant" in file.lower() or "nonredundant" in file.lower()) and ".vcf" in file and basename(root) != "nr":
                        shutil.move(os.path.join(root,file), os.path.dirname(root)+"/nr/")
                    elif ("_P." in file or "partial" in file.lower()) and ".vcf" in file and basename(root) != "partial":
                        shutil.move(os.path.join(root,file), os.path.dirname(root)+"/partial/")
                    elif ".vcf" in file :
                        pass
                    else:
                        f.write("\n Incorrect file extension in "+os.path.join(root,file)+"... deleting.")
                        os.remove(os.path.join(root,file))
                
        if (check_change):
            pass
        else:
            loop=False

    print("Files distribution based on file extension and name checked.\n")

    for pathogen in liste_pat:
        for sample in queryset:
            if sample.organism == pathogen:
                find_duplicate(f, sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen)


    print("Presence of duplicate files for registered sample checked.\n")


    for pathogen in liste_pat:
        # Path definition based on genome type
        for sample in queryset:
            if sample.organism == pathogen:

                PATH_FASTA_C, PATH_FASTA_NR, PATH_FASTA_P, PATH_GFF_C, PATH_GFF_NR, PATH_GFF_P, PATH_IMG_GFF_C, PATH_IMG_GFF_NR, PATH_IMG_GFF_P, PATH_VCF_C, PATH_VCF_NR, PATH_VCF_P, PATH_IMG_VCF_C, PATH_IMG_VCF_NR, PATH_IMG_VCF_P = {},{},{},{},{},{},{},{},{},{},{},{},{},{},{}

                # We get the existence of a file with the sample name of each directory possible, base on the value returned (True -> a file exst) for each path we move them
                PATH_FASTA_C[MPSQ_FILES + pathogen + '/fasta/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/fasta/complete/')
                PATH_FASTA_NR[MPSQ_FILES + pathogen + '/fasta/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/fasta/nr/')
                PATH_FASTA_P[MPSQ_FILES + pathogen + '/fasta/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/fasta/partial/')

                PATH_GFF_C[MPSQ_FILES + pathogen + '/gff/files/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/files/complete/')
                PATH_GFF_NR[MPSQ_FILES + pathogen + '/gff/files/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/files/nr/')
                PATH_GFF_P[MPSQ_FILES + pathogen + '/gff/files/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/files/partial/')

                PATH_IMG_GFF_C[MPSQ_FILES + pathogen + '/gff/img/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/img/complete/')
                PATH_IMG_GFF_NR[MPSQ_FILES + pathogen + '/gff/img/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/img/nr/')
                PATH_IMG_GFF_P[MPSQ_FILES + pathogen + '/gff/img/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/img/partial/')

                PATH_VCF_C[MPSQ_FILES + pathogen + '/vcf/files/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/files/complete/')
                PATH_VCF_NR[MPSQ_FILES + pathogen + '/vcf/files/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/files/nr/')
                PATH_VCF_P[MPSQ_FILES + pathogen + '/vcf/files/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/files/partial/')

                PATH_IMG_VCF_C[MPSQ_FILES + pathogen + '/vcf/img/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/img/complete/')
                PATH_IMG_VCF_NR[MPSQ_FILES + pathogen + '/vcf/img/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/img/nr/')
                PATH_IMG_VCF_P[MPSQ_FILES + pathogen + '/vcf/img/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/img/partial/')

                if sorted(sample.genome_type.split("_")) == ["Complete"] or not sample.genome_type or sample.genome_type in missing_values: #Si valeur genome_type absente c'est complete par défaut
                    #Fasta
                    if next(iter(PATH_FASTA_C.values())) != None :
                        if next(iter(PATH_FASTA_NR.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_NR.values()))+"... deleting.")
                            os.remove(next(iter(PATH_FASTA_NR.values())))
                        if next(iter(PATH_FASTA_P.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_P.values()))+"... deleting.")
                            os.remove(next(iter(PATH_FASTA_P.values())))
                    else:
                        change = False
                        if next(iter(PATH_FASTA_NR.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_FASTA_NR.values())), next(iter(PATH_FASTA_C.keys())))
                        if next(iter(PATH_FASTA_P.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_FASTA_P.values())), next(iter(PATH_FASTA_C.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_P.values()))+"... deleting.")
                                os.remove(next(iter(PATH_FASTA_P.values())))
                    
                    #GFF
                    if next(iter(PATH_GFF_C.values())) != None :
                        if next(iter(PATH_GFF_NR.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_NR.values()))+"... deleting.")
                            os.remove(next(iter(PATH_GFF_NR.values())))
                        if next(iter(PATH_GFF_P.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_P.values()))+"... deleting.")
                            os.remove(next(iter(PATH_GFF_P.values())))
                    else:
                        change = False
                        if next(iter(PATH_GFF_NR.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_GFF_NR.values())), next(iter(PATH_GFF_C.keys())))
                        if next(iter(PATH_GFF_P.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_GFF_P.values())), next(iter(PATH_GFF_C.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_P.values()))+"... deleting.")
                                os.remove(next(iter(PATH_GFF_P.values())))
                    #Since GFF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_GFF_NR.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_NR.values())))
                    if next(iter(PATH_IMG_GFF_P.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_P.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_GFF_C.values())) != None and next(iter(PATH_GFF_C.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_C.values())))
                    
                    #VCF
                    if next(iter(PATH_VCF_C.values())) != None :
                        if next(iter(PATH_VCF_NR.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_NR.values()))+"... deleting.")
                            os.remove(next(iter(PATH_VCF_NR.values())))
                        if next(iter(PATH_VCF_P.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_P.values()))+"... deleting.")
                            os.remove(next(iter(PATH_VCF_P.values())))
                    else:
                        change = False
                        if next(iter(PATH_VCF_NR.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_VCF_NR.values())), next(iter(PATH_VCF_C.keys())))
                        if next(iter(PATH_VCF_P.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_VCF_P.values())), next(iter(PATH_VCF_C.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_P.values()))+"... deleting.")
                                os.remove(next(iter(PATH_VCF_P.values())))
                    #Since VCF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_VCF_NR.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_NR.values())))
                    if next(iter(PATH_IMG_VCF_P.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_P.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_VCF_C.values())) != None and next(iter(PATH_VCF_C.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_C.values())))

                if sorted(sample.genome_type.split("_")) == ["Non-redundant"] :
                    #Fasta
                    if next(iter(PATH_FASTA_NR.values())) != None :
                        if next(iter(PATH_FASTA_C.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_C.values()))+"... deleting.")
                            os.remove(next(iter(PATH_FASTA_C.values())))
                        if next(iter(PATH_FASTA_P.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_P.values()))+"... deleting.")
                            os.remove(next(iter(PATH_FASTA_P.values())))
                    else:
                        change = False
                        if next(iter(PATH_FASTA_C.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_FASTA_C.values())), next(iter(PATH_FASTA_NR.keys())))
                        if next(iter(PATH_FASTA_P.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_FASTA_P.values())), next(iter(PATH_FASTA_NR.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_P.values()))+"... deleting.")
                                os.remove(next(iter(PATH_FASTA_P.values())))
                    
                    #GFF
                    if next(iter(PATH_GFF_NR.values())) != None :
                        if next(iter(PATH_GFF_C.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_C.values()))+"... deleting.")
                            os.remove(next(iter(PATH_GFF_C.values())))
                        if next(iter(PATH_GFF_P.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_P.values()))+"... deleting.")
                            os.remove(next(iter(PATH_GFF_P.values())))
                    else:
                        change = False
                        if next(iter(PATH_GFF_C.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_GFF_C.values())), next(iter(PATH_GFF_NR.keys())))
                        if next(iter(PATH_GFF_P.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_GFF_P.values())), next(iter(PATH_GFF_NR.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_P.values()))+"... deleting.")
                                os.remove(next(iter(PATH_GFF_P.values())))
                    #Since GFF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_GFF_C.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_C.values())))
                    if next(iter(PATH_IMG_GFF_P.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_P.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_GFF_NR.values())) != None and next(iter(PATH_GFF_NR.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_NR.values())))
                    
                    #VCF
                    if next(iter(PATH_VCF_NR.values())) != None :
                        if next(iter(PATH_VCF_C.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_C.values()))+"... deleting.")
                            os.remove(next(iter(PATH_VCF_C.values())))
                        if next(iter(PATH_VCF_P.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_P.values()))+"... deleting.")
                            os.remove(next(iter(PATH_VCF_P.values())))
                    else:
                        change = False
                        if next(iter(PATH_VCF_C.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_VCF_C.values())), next(iter(PATH_VCF_NR.keys())))
                        if next(iter(PATH_VCF_P.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_VCF_P.values())), next(iter(PATH_VCF_NR.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_P.values()))+"... deleting.")
                                os.remove(next(iter(PATH_VCF_P.values())))
                    #Since VCF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_VCF_C.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_C.values())))
                    if next(iter(PATH_IMG_VCF_P.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_P.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_VCF_NR.values())) != None and next(iter(PATH_VCF_NR.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_NR.values())))
                    
                if sorted(sample.genome_type.split("_")) == ["Partial"] :
                    #Fasta
                    if next(iter(PATH_FASTA_P.values())) != None : 
                        if next(iter(PATH_FASTA_C.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_C.values()))+"... deleting.")
                            os.remove(next(iter(PATH_FASTA_C.values())))
                        if next(iter(PATH_FASTA_NR.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_NR.values()))+"... deleting.")
                            os.remove(next(iter(PATH_FASTA_NR.values())))
                    else:
                        change = False
                        if next(iter(PATH_FASTA_C.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_FASTA_C.values())), next(iter(PATH_FASTA_P.keys())))
                        if next(iter(PATH_FASTA_NR.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_FASTA_NR.values())), next(iter(PATH_FASTA_P.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_NR.values()))+"... deleting.")
                                os.remove(next(iter(PATH_FASTA_NR.values())))
                    
                    #GFF
                    if next(iter(PATH_GFF_P.values())) != None :
                        if next(iter(PATH_GFF_C.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_C.values()))+"... deleting.")
                            os.remove(next(iter(PATH_GFF_C.values())))
                        if next(iter(PATH_GFF_NR.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_NR.values()))+"... deleting.")
                            os.remove(next(iter(PATH_GFF_NR.values())))
                    else:
                        change = False
                        if next(iter(PATH_GFF_C.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_GFF_C.values())), next(iter(PATH_GFF_P.keys())))
                        if next(iter(PATH_GFF_NR.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_GFF_NR.values())), next(iter(PATH_GFF_P.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_NR.values()))+"... deleting.")
                                os.remove(next(iter(PATH_GFF_NR.values())))
                    #Since GFF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_GFF_C.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_C.values())))
                    if next(iter(PATH_IMG_GFF_NR.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_NR.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_GFF_P.values())) != None and next(iter(PATH_GFF_P.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_P.values())))
                    
                    #VCF
                    if next(iter(PATH_VCF_P.values())) != None :
                        if next(iter(PATH_VCF_C.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_C.values()))+"... deleting.")
                            os.remove(next(iter(PATH_VCF_C.values())))
                        if next(iter(PATH_VCF_NR.values())) != None :
                            f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_NR.values()))+"... deleting.")
                            os.remove(next(iter(PATH_VCF_NR.values())))
                    else:
                        change = False
                        if next(iter(PATH_VCF_C.values())) != None :
                            change = True
                            shutil.move(next(iter(PATH_VCF_C.values())), next(iter(PATH_VCF_P.keys())))
                        if next(iter(PATH_VCF_NR.values())) != None :
                            if change == False:
                                shutil.move(next(iter(PATH_VCF_NR.values())), next(iter(PATH_VCF_P.keys())))
                            else:
                                f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_NR.values()))+"... deleting.")
                                os.remove(next(iter(PATH_VCF_NR.values())))
                    #Since VCF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_VCF_C.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_C.values())))
                    if next(iter(PATH_IMG_VCF_NR.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_NR.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_VCF_P.values())) != None and next(iter(PATH_VCF_P.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_P.values())))
                    
                if sorted(sample.genome_type.split("_")) == ["Complete","Non-redundant"]:
                    #Fasta
                    if next(iter(PATH_FASTA_P.values())) != None :
                        change = False
                        if next(iter(PATH_FASTA_C.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_FASTA_P.values())), next(iter(PATH_FASTA_C.keys())))
                        if next(iter(PATH_FASTA_NR.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_FASTA_P.values())), next(iter(PATH_FASTA_NR.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_FASTA_P.values())))
                    
                    #GFF
                    if next(iter(PATH_GFF_P.values())) != None :
                        change = False
                        if next(iter(PATH_GFF_C.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_GFF_P.values())), next(iter(PATH_GFF_C.keys())))
                        if next(iter(PATH_GFF_NR.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_GFF_P.values())), next(iter(PATH_GFF_NR.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_GFF_P.values())))
                    #Since GFF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_GFF_P.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_P.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_GFF_C.values())) != None and next(iter(PATH_GFF_C.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_C.values())))
                    
                    if next(iter(PATH_IMG_GFF_NR.values())) != None and next(iter(PATH_GFF_NR.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_NR.values())))
                    
                    #VCF
                    if next(iter(PATH_VCF_P.values())) != None :
                        change = False
                        if next(iter(PATH_VCF_C.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_VCF_P.values())), next(iter(PATH_VCF_C.keys())))
                        if next(iter(PATH_VCF_NR.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_VCF_P.values())), next(iter(PATH_VCF_NR.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_VCF_P.values())))
                    #Since VCF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_VCF_P.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_P.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_VCF_C.values())) != None and next(iter(PATH_VCF_C.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_C.values())))
                    if next(iter(PATH_IMG_VCF_NR.values())) != None and next(iter(PATH_VCF_NR.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_NR.values())))

                if sorted(sample.genome_type.split("_")) == ["Complete","Partial"]:
                    #Fasta
                    if next(iter(PATH_FASTA_NR.values())) != None :
                        change = False
                        if next(iter(PATH_FASTA_C.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_FASTA_NR.values())), next(iter(PATH_FASTA_C.keys())))
                        if next(iter(PATH_FASTA_P.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_FASTA_NR.values())), next(iter(PATH_FASTA_P.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_FASTA_NR.values())))
                    
                    #GFF
                    if next(iter(PATH_GFF_NR.values())) != None :
                        change = False
                        if next(iter(PATH_GFF_C.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_GFF_NR.values())), next(iter(PATH_GFF_C.keys())))
                        if next(iter(PATH_GFF_P.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_GFF_NR.values())), next(iter(PATH_GFF_P.keys())))
                        
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_GFF_NR.values())))
                    #Since GFF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_GFF_NR.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_NR.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_GFF_C.values())) != None and next(iter(PATH_GFF_C.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_C.values())))
                    if next(iter(PATH_IMG_GFF_P.values())) != None and next(iter(PATH_GFF_P.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_P.values())))
                    
                    #VCF
                    if next(iter(PATH_VCF_NR.values())) != None :
                        change = False
                        if next(iter(PATH_VCF_C.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_VCF_NR.values())), next(iter(PATH_VCF_C.keys())))
                        if next(iter(PATH_VCF_P.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_VCF_NR.values())), next(iter(PATH_VCF_P.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_VCF_NR.values())))
                    #Since VCF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_VCF_NR.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_NR.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_VCF_C.values())) != None and next(iter(PATH_VCF_C.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_C.values())))
                    if next(iter(PATH_IMG_VCF_P.values())) != None and next(iter(PATH_VCF_P.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_P.values())))

                if sorted(sample.genome_type.split("_")) == ["Non-redundant","Partial"]:
                    #Fasta
                    if next(iter(PATH_FASTA_C.values())) != None :
                        change = False
                        if next(iter(PATH_FASTA_NR.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_FASTA_C.values())), next(iter(PATH_FASTA_NR.keys())))
                        if next(iter(PATH_FASTA_P.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_FASTA_C.values())), next(iter(PATH_FASTA_P.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_FASTA_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_FASTA_C.values())))
                    
                    #GFF
                    if next(iter(PATH_GFF_C.values())) != None :
                        change = False
                        if next(iter(PATH_GFF_NR.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_GFF_C.values())), next(iter(PATH_GFF_NR.keys())))
                        if next(iter(PATH_GFF_P.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_GFF_C.values())), next(iter(PATH_GFF_P.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_GFF_C.values())))
                    #Since GFF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_GFF_C.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_C.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_GFF_NR.values())) != None and next(iter(PATH_GFF_NR.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_NR.values())))
                    if next(iter(PATH_IMG_GFF_P.values())) != None and next(iter(PATH_GFF_P.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_P.values())))
                    
                    #VCF
                    if next(iter(PATH_VCF_C.values())) != None :
                        change = False
                        if next(iter(PATH_VCF_NR.values())) == None :
                            change = True
                            shutil.move(next(iter(PATH_VCF_C.values())), next(iter(PATH_VCF_NR.keys())))
                        if next(iter(PATH_VCF_P.values())) == None :
                            if change == False:
                                shutil.move(next(iter(PATH_VCF_C.values())), next(iter(PATH_VCF_P.keys())))

                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_VCF_C.values())))
                    #Since VCF img are generated, we remove them directly if not corresponding 
                    if next(iter(PATH_IMG_VCF_C.values())) != None :
                        f.write("\n Incorrect file based on genome type "+next(iter(PATH_IMG_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_C.values())))
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_VCF_NR.values())) != None and next(iter(PATH_VCF_NR.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_NR.values())))
                    if next(iter(PATH_IMG_VCF_P.values())) != None and next(iter(PATH_VCF_P.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_P.values())))

                if sorted(sample.genome_type.split("_")) == ["Complete","Non-redundant","Partial"]:            
                    #If img is there but no file correspongding we remove
                    if next(iter(PATH_IMG_GFF_C.values())) != None and next(iter(PATH_GFF_C.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_C.values())))
                    if next(iter(PATH_IMG_GFF_NR.values())) != None and next(iter(PATH_GFF_NR.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_NR.values())))
                    if next(iter(PATH_IMG_GFF_P.values())) != None and next(iter(PATH_GFF_P.values())) == None:
                        f.write("\n Incorrect file because no gff corresponding "+next(iter(PATH_IMG_GFF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_GFF_P.values())))
                    
                    if next(iter(PATH_IMG_VCF_C.values())) != None and next(iter(PATH_VCF_C.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_C.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_C.values())))
                    if next(iter(PATH_IMG_VCF_NR.values())) != None and next(iter(PATH_VCF_NR.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_NR.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_NR.values())))
                    if next(iter(PATH_IMG_VCF_P.values())) != None and next(iter(PATH_VCF_P.values())) == None:
                        f.write("\n Incorrect file because no vcf corresponding "+next(iter(PATH_IMG_VCF_P.values()))+"... deleting.")
                        os.remove(next(iter(PATH_IMG_VCF_P.values())))

    print("Files distribution based on genome type checked.\n")

    for pathogen in liste_pat:
        # Path definition based on genome type
        for sample in queryset:
            if sample.organism == pathogen:

                PATH_FASTA_C, PATH_FASTA_NR, PATH_FASTA_P, PATH_GFF_C, PATH_GFF_NR, PATH_GFF_P, PATH_VCF_C, PATH_VCF_NR, PATH_VCF_P = {},{},{},{},{},{},{},{},{}

                # We get the existence of a file with the sample name of each directory possible, base on the value returned (True -> a file exst) for each path we move them
                PATH_FASTA_C[MPSQ_FILES + pathogen + '/fasta/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/fasta/complete/')
                PATH_FASTA_NR[MPSQ_FILES + pathogen + '/fasta/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/fasta/nr/')
                PATH_FASTA_P[MPSQ_FILES + pathogen + '/fasta/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/fasta/partial/')

                PATH_GFF_C[MPSQ_FILES + pathogen + '/gff/files/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/files/complete/')
                PATH_GFF_NR[MPSQ_FILES + pathogen + '/gff/files/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/files/nr/')
                PATH_GFF_P[MPSQ_FILES + pathogen + '/gff/files/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/gff/files/partial/')

                PATH_VCF_C[MPSQ_FILES + pathogen + '/vcf/files/complete/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/files/complete/')
                PATH_VCF_NR[MPSQ_FILES + pathogen + '/vcf/files/nr/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/files/nr/')
                PATH_VCF_P[MPSQ_FILES + pathogen + '/vcf/files/partial/'] = find_path(sample.sample_name+'[._-]?[!0-9]*', MPSQ_FILES + pathogen + '/vcf/files/partial/')


                if next(iter(PATH_FASTA_C.values())) != None:
                    if basename(next(iter(PATH_FASTA_C.values()))) != sample.sample_name+"_C.fasta":
                        os.rename(next(iter(PATH_FASTA_C.values())), next(iter(PATH_FASTA_C.keys()))+sample.sample_name+"_C.fasta")
                if next(iter(PATH_FASTA_NR.values())) != None:
                    if basename(next(iter(PATH_FASTA_NR.values()))) != sample.sample_name+"_NR.fasta":
                        os.rename(next(iter(PATH_FASTA_NR.values())), next(iter(PATH_FASTA_NR.keys()))+sample.sample_name+"_NR.fasta")
                if next(iter(PATH_FASTA_P.values())) != None:
                    if basename(next(iter(PATH_FASTA_P.values()))) != sample.sample_name+"_P.fasta":
                        os.rename(next(iter(PATH_FASTA_P.values())), next(iter(PATH_FASTA_P.keys()))+sample.sample_name+"_P.fasta")

                if next(iter(PATH_GFF_C.values())) != None:
                    if basename(next(iter(PATH_GFF_C.values()))) != sample.sample_name+"_C.gff":
                        os.rename(next(iter(PATH_GFF_C.values())), next(iter(PATH_GFF_C.keys()))+sample.sample_name+"_C.gff")
                if next(iter(PATH_GFF_NR.values())) != None:
                    if basename(next(iter(PATH_GFF_NR.values()))) != sample.sample_name+"_NR.gff":
                        os.rename(next(iter(PATH_GFF_NR.values())), next(iter(PATH_GFF_NR.keys()))+sample.sample_name+"_NR.gff")
                if next(iter(PATH_GFF_P.values())) != None:
                    if basename(next(iter(PATH_GFF_P.values()))) != sample.sample_name+"_P.gff":
                        os.rename(next(iter(PATH_GFF_P.values())), next(iter(PATH_GFF_P.keys()))+sample.sample_name+"_P.gff")

                if next(iter(PATH_VCF_C.values())) != None:
                    if basename(next(iter(PATH_VCF_C.values()))) != sample.sample_name+"_C.vcf":
                        os.rename(next(iter(PATH_VCF_C.values())), next(iter(PATH_VCF_C.keys()))+sample.sample_name+"_C.vcf")
                if next(iter(PATH_VCF_NR.values())) != None:
                    if basename(next(iter(PATH_VCF_NR.values()))) != sample.sample_name+"_NR.vcf":
                        os.rename(next(iter(PATH_VCF_NR.values())), next(iter(PATH_VCF_NR.keys()))+sample.sample_name+"_NR.vcf")
                if next(iter(PATH_VCF_P.values())) != None:
                    if basename(next(iter(PATH_VCF_P.values()))) != sample.sample_name+"_P.vcf":
                        os.rename(next(iter(PATH_VCF_P.values())), next(iter(PATH_VCF_P.keys()))+sample.sample_name+"_P.vcf")
               
    print("Files renaming of registered samples over.\n")

    f.close() 
    if  os.stat("checkup.log").st_size == 0:
        print("Check-up over. No deletion of files recorded.\n")
        os.remove("checkup.log") 
    else:
        print("Check-up over. Find all removing instances in the checkup.log file.\n")
     
    return