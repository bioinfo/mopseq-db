## Preparing data for import into MoPSeq-DB

### Prepare the environment

```
conda create -n env python=3.9
conda activate env
conda install -c bioconda mafft 
conda install -c bioconda java-jdk 
conda install -c bioconda phyml 
conda install -c bioconda raxml 
pip install -r requirements.txt
```

Download and extract the jModeltest v.2.1.10 folder from the following link: https://github.com/ddarriba/jmodeltest2/releases/tag/v2.1.10r20160303
```
tar -xf jmodeltest-2.1.10.tar.gz

```

Place the extracted folder in your env/ directory.

Provided scripts are designed to be used on a PBS based cluster, you may have to dapat them for use elsewhere.