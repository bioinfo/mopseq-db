# 1. Introduction

MoPSeq-DB is a bioinformatics web application designed for the visualization and analysis of mollusc pathogen sequence data, with a specific emphasis on genome structure, variations, and phylogeny.

Some features of MoPSeq-DB include:

|  |  |
| ------ | ------ |
| ![graph](static/img/nav_db.gif)       | Pathogens nucleotide sequences are referenced with clean, organized metadata and can be visualized.       |
| Sequence data, annotation and metadata files can be downloaded.       | ![graph](static/img/dl.gif)       |
| ![graph](static/img/graph.gif)        | Interactive visualizations of genomic structures and intra-individual variations.  |
| Interactive visualization of pre-built phylogenetic tree files.        | ![graph](static/img/tree.gif)       |

# 2. Installation

## 2.1. Docker installation

Here is the easiest way to run MopSeq-DB using Docker:
```
# Get MopSeq-DB software
cd /a/directory/of/your/choice
git clone https://gitlab.ifremer.fr/bioinfo/mopseq-db.git
cd mopseq-db
# Get test data set
./get-test-data.sh
# Build the image
docker-compose build
# Start the MopSeq-DB container
docker-compose up -d
```

Open your web browser and go to: http://127.0.0.1:8000

When you are finished using the server, simply stop it using:
```
docker-compose down
```

## 2.2. Manual installation

### 2.2.1. Prepare the environment

```
conda create -n env python=3.9
conda activate env
pip install -r requirements.txt
```

### 2.2.2. Set up environment variables for local or production deployment

For localhost, run the following commands:
```
export MPSQ_PROD="False"
export ALLOWED_HOSTS="127.0.0.1"
```

For production, run the following command:
```
export MPSQ_PROD="True"
```

### 2.2.3. Set up access to data

To set the absolute path to the folder where all the data files are stored and the path to the db.sqlite file, use the following commands:
```
export MPSQ_FILES="Your/path/to/the/directory/data_files/"
export MPSQ_BDD="Your/path/to/the/SQL/file/db.sqlite3"
export VIRTUAL_ENV="Your/path/to/your/virtual/env"
```

### 2.2.4. Set up checking (optional)

Run the following commands to perform the setup checking:
```
python manage.py makemigrations
python manage.py migrate
```

### 2.2.5. Starting the application

To start the application, run the following command:
```
python manage.py runserver
```

Then, open your web browser and go to: http://127.0.0.1:8000

# 3. Importing data, generating structure and variations images, creating a new pathogen entry

MoPSeq-DB provides a convenient script for uploading data into the database. Use the following command to access the help menu for the script:
```
python MPSQ-upd.py --help
```

# 4. Sqlite3 vs Postgres

By default, MoPSeq-DB is configured to use a Sqlite3 database, which is suitable for testing purposes. However, you have the flexibility to use any other relational database by making appropriate changes to the MoPSeq-DB code.

To switch to a different database, you can refer to the 'env.test' and 'env.db' configuration files. The 'env.test' file is used by default for configuring a 'sqlite3' database driver. However, you can switch to a Postgres database driver by using the 'env.db' configuration file. Make sure to edit this file and provide the correct database name, login, and password. If you choose to use Postgres, you will need to set up, configure, and run your own Postgres database.

It is important to note that either the 'env.test' or 'env.db' file is automatically called from the 'docker-compose.yml' file, which is recommended for Docker installation. If you are running MoPSeq-DB directly from the command-line using 'python manage.py runserver', please make sure to adapt the configuration accordingly.

# Credits

MoPSeq-DB is an initiative that was started in 2021 by the French Research Institute for Exploitation of the Sea (Ifremer), with support from the European Commission. 

The MoPSeq-DB software was developed by [Jean-Christophe Mouren](https://www.linkedin.com/in/jean-christophe-mouren-116b881b6/?originalSubdomain=fr) and [Clementine Battistel](https://www.linkedin.com/in/cl%C3%A9mentine-battistel-967356280) under the supervision of [Maude Jacquot](https://annuaire.ifremer.fr/cv/27588/) from the [ASIM team](https://asim.ifremer.fr/) at Ifremer in la Tremblade, France. 

Other contributors to the project include: [Germain Chevignon](https://annuaire.ifremer.fr/cv/27112/), [Benjamin Morga](https://annuaire.ifremer.fr/cv/16995/), [Camille Pelletier](https://annuaire.ifremer.fr/cv/23970/), [Céline Garcia](https://annuaire.ifremer.fr/cv/16063/), [Lydie Canier](https://annuaire.ifremer.fr/cv/23479/) and [Isabelle Arzul](https://annuaire.ifremer.fr/cv/16827/). 

Technical support for Dockerization and web deployment was provided by [Pauline Auffret](https://annuaire.ifremer.fr/cv/29214/en/), [Laura Leroi](https://annuaire.ifremer.fr/cv/21673/), [Patrick Durand](https://annuaire.ifremer.fr/cv/24218/), Erwan Bodere and [Leo Bruvry-Lagadec](https://annuaire.ifremer.fr/cv/26108/).

# Contributions

We encourage contributions to the MoPSeq-DB software. 

If you have any questions or need assistance, you can use the "Issues" feature on the project's GitLab page to submit your inquiries.

Alternatively, you can fork the project, make your own developments, and then submit a pull request to contribute your changes back to the main repository.


# Support/Contact

If you need further support or have any other inquiries, please feel free to get in touch with [Maude Jacquot](https://annuaire.ifremer.fr/cv/27588/).

