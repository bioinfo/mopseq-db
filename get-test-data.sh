#!/bin/bash

# ==================================================================
#
# Script to download a simple data set to test application.
# By default, this data set is installed in ../mopseq-db-test-data
#
# ==================================================================

# --------
# FUNCTION: print out a simple message on stderr 
function errorMsg(){
  printf "ERROR: $* \n" >&2
}

# --------
# FUNCTION: print out a simple message on stdout
function infoMsg(){
  printf "$* \n"
}
# --------
# FUNCTION: figure out whther or not a command exists.
#  arg1: a command name.
#  return: 0 if success 
hasCommand () {
    command -v "$1" >/dev/null 2>&1
}

# --------
# FUNCTION: download a file from a remote server.
#           URL is downloaded using curl or wget, saved in
#           current directory and named using provided local
#           file name. Function is capable of resuming a
#           previous aborted job.
#  arg1: local file name
#  arg2: URL
#  return: 0 if success
function downloadFile(){
  filename=$1
  url=$2
  #Use verbose-less download mode to avoid having huge log.
  #Use curl first, since it's more easy to get only error 
  #messages if any
  if hasCommand curl; then
    CMD="curl -sSL -o $filename -C - $url"
  elif hasCommand wget; then
    CMD="wget -c -q $url -O $filename"
  else
    errorMsg "Could not find curl nor wget, please install one of them."
    return 1
  fi
  infoMsg $CMD
  eval $CMD && return 0 || return 1
}

# = Main =====================================================================
cd ..
DATA_DIR=mopseq-db-test-data
mkdir $DATA_DIR
cd $DATA_DIR

downloadFile data.zip "https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/MopSeq-DB/test-data/data_files_final.zip"
if [ $? -eq 0 ]; then
  unzip data.zip
  rm data.zip
  echo "SUCCESS"
else
  echo "ERROR"
  exit 1
fi


