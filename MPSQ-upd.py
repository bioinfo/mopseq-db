######################################################################################
# MoPSeq-DB maintenance software.
# Author: Jean-Christophe Mouren, Clémentine Battistel, Maude Jacquot
# Creation date: 2021-07-01
# Last modified: 2024-02-22 
#########################################################################################################

import os, typer
from pathlib import Path

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mopseq.settings") ###sert à lancer l'environnement console de django
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from features.models import Pathogen

from update.check_database import check_up
from update.gen_gff_graph import gen_gff_graph
from update.gen_vcf_graph import gen_vcf_graph
from update.gen_phylotree import gen_phylotree


def main(
    gff: bool = typer.Option(False, "--gff", help="Generate GFF visualization graph files (.html)"),
    vcf: bool = typer.Option(False, "--vcf", help="Generate VCF visualization graph files (.html)"),
    phylo: bool = typer.Option(False, "--phylo", help="Launch a pipeline to generate the pathogen phylogenetic tree. You need to make sure the database in up to date metadata first (run manage.py update_database to do so)."),

    pathogen: str = typer.Option("", help="The name of the pathogen (as recorded in the database) to work on"),
    all: bool = typer.Option(False, "--all", help="Work on all the pathogens recorded in the database"),

    overwrite: bool = typer.Option(False, "--overwrite", help="Overwrite existing GFF/VCF files"),

    checkup: bool = typer.Option(False, "--checkup", help="Check files distribution of referenced pathogens and try to correct it. Also remove pathogen from database if data_files repository not found. Writes checkup.log file."),
    ):

    if pathogen and all:
        typer.echo("You have to choose between --pathogen and --all. Type --help for more info.")
        return
    
    if checkup:
        queryset = Pathogen.objects.all()
        check_up(queryset=queryset)

        #if metadata == Path(".") and not gff and not vcf and not phylo:
        #    return

    if pathogen:
        queryset = Pathogen.objects.filter(organism=pathogen)
        liste_pat = [pathogen]
        if not queryset :
        #    if metadata :
        #        typer.echo("Pathogen not found... Assuming wish of new entry due to --metadata option.")
        #    else:
            typer.echo("Pathogen not found")
            return
    elif all:
        queryset = Pathogen.objects.all()

        dic = Pathogen.objects.order_by().values('organism').distinct()
        liste_pat = []
        try:
            for i in dic:
                values = i.values()
                values_iterator = iter(values)
                organism_value = next(values_iterator)
                liste_pat.append(organism_value)      
        except Exception:
            pass
        
        liste_pat.append("ALL") #Si jamais il n'y a qu'un seul pathogène enregistré dans la base

    else:
        typer.echo("--pathogen or --all option required. Type --help for more info.")
        return
    
    if gff : 
        if pathogen and pathogen=="Vibrio aestuarianus":
            print("Please use update/BigGenome_gff_image_generator.py to generate gff images for Vibrio aestuarianus.\nRun python update/BigGenome_gff_image_generator.py --help for more details.")
        else:
            print("Beginning of the gff images generation...\n")
            gen_gff_graph(queryset=queryset, liste_pat=liste_pat, overwrite=overwrite)

    if vcf : 
        print("Beginning of the vcf images generation...\n")
        gen_vcf_graph(queryset=queryset, liste_pat=liste_pat, overwrite=overwrite)

    if phylo : 
        gen_phylotree(queryset=queryset, liste_pat=liste_pat)
   
if __name__ == "__main__":
    # Verification than global path variable are correctly set up
    MPSQ_FILES = os.getenv('MPSQ_FILES')
    MPSQ_BDD = os.getenv('MPSQ_BDD')

    if MPSQ_FILES is None or MPSQ_BDD is None:
        typer.echo("The MPSQ_FILES or MPSQ_BDD variable path towards data_files directory and the sql file of MOPSEQ-DB are not defined. \nPlease define them.")
        quit()
    else :
        if os.path.exists(MPSQ_FILES) == False:
            typer.echo("MPSQ_FILES path variable to the data_files directory of MOPSEQ-DB not correct. Please define it again.\n")
            quit()
        if os.path.exists(MPSQ_BDD) == False:
            typer.echo("MPSQ_BDD path variable to the sql file of MOPSEQ-DB not correct. Please define it again\n")
            quit()

    MPSQ_PROD = os.getenv('MPSQ_PROD')

    if str(MPSQ_PROD) not in ["True", "False"]:
        typer.echo("You have to setup the MPSQ_PROD variable with True or False.")
        quit()

    """
    VIRTUAL_ENV = os.getenv('VIRTUAL_ENV')
    path = str(VIRTUAL_ENV)
    #if VIRTUAL_ENV is None:
    #    typer.echo("The VIRTUAL_ENV variable path towards your virtual env directory is not defined. \nPlease define it.")
    #    quit()
    #else :
    #    if os.path.exists(VIRTUAL_ENV) == False:
    #        typer.echo("VIRTUAL_ENV path variable to the virtual env directory of MOPSEQ-DB not correct. Please define it again.\n")
    #        quit()
    """

    # Warning message to the user 
    typer.echo("""
######################################################################################
MoPSeq-DB maintenance software.
Usage: MPSQ-upd.py [Options]               

Before running, please make sure :
- That each name of fasta, gff or vcf files stored begins with the name corresponding to the attribute "sample_name" in the database. 
- That these files are correctly distributed in the folders within folder /data_files/ based on their genome types.

Type --help for more info.
""")

    typer.run(main)
