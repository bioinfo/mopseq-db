# Dockerfile aims at packaging the MopSeq-DB Django application
# Get a Linux Python 3.10 image
ARG IMAGE_REGISTRY="gitlab-registry.ifremer.fr/ifremer-commons/docker/images/"
ARG DOCKER_IMAGE="python:3.10-slim-buster"
FROM $IMAGE_REGISTRY$DOCKER_IMAGE
ENV PYTHONUNBUFFERED 1

################################################################################
# DEFAULT USER FOR TEST PURPOSE
################################################################################
ARG DK_USER="django"
ARG DK_USER_ID="5000"
ARG DK_GROUP="django"
ARG DK_GROUP_ID="5010"

################################################################################
# UPDATE OS
################################################################################

RUN rm -rf /var/lib/apt/lists/* && \
    apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y purge --auto-remove -o APT::AutoRemove::RecommendsImportant=false

################################################################################
# NON PRIVILEGED USER/GROUP CREATION
################################################################################
RUN addgroup --system --gid $DK_GROUP_ID $DK_GROUP && \
    adduser --system --disabled-login --ingroup $DK_GROUP --no-create-home --gecos "$DK_USER user" --shell /bin/false --uid $DK_USER_ID $DK_USER && \
    echo "Creating group ${GROUPNAME}:${GROUPID} and user ${USERNAME}:${USERID}"

################################################################################
# NON PRIVILEGED NGINX INSTALLATION
################################################################################

RUN apt-get install --no-install-recommends --no-install-suggests -y gnupg2 ca-certificates && \
    NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62; \
    found=''; \
    for server in \
        ha.pool.sks-keyservers.net \
        hkp://keyserver.ubuntu.com:80 \
        hkp://p80.pool.sks-keyservers.net:80 \
        pgp.mit.edu \
    ; do \
        echo "Fetching GPG key $NGINX_GPGKEY from $server"; \
        apt-key adv --keyserver "$server" --keyserver-options timeout=30 --recv-keys "$NGINX_GPGKEY" && found=yes && break; \
    done; \
    test -z "$found" && echo >&2 "error: failed to fetch GPG key $NGINX_GPGKEY" && exit 1; \
    apt-get remove --purge --auto-remove -y gnupg2 && rm -rf /var/lib/apt/lists/* && \
    echo "deb http://nginx.org/packages/debian buster nginx" | tee /etc/apt/sources.list.d/nginx.list && \
    apt-get update && \
    apt-get -y install nginx=1.18.0-1~buster git && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    chown -R $DK_USER:0 /var/cache/nginx && \
    chmod -R g+w /var/cache/nginx && \
    chown -R $DK_USER:0 /etc/nginx && \
    chmod -R g+w /etc/nginx

################################################################################
# PYTHON DEPENDENCIES INSTALLATION
################################################################################

RUN apt-get install --no-install-recommends --no-install-suggests -y \
    python-pip \
    build-essential gettext \
    procps iputils-ping telnet iptables sqlite3 libsqlite3-dev && \
    pip install --upgrade pip && \
    pip install uwsgi && \
    apt-get remove --purge --auto-remove -y \
    python-pip \
    build-essential

################################################################################
# Copy MopSeq-DB code into the image
################################################################################

COPY --chown=$DK_USER:$DK_GROUP . /app
RUN chmod +x /app/entrypoint.sh && \
    chmod a+w /app/features/migrations

# Set working directory
WORKDIR /app

# Install depedencies
RUN pip install -r requirements.txt

#############################################################################
#  WSGI CONFIGURATION
#############################################################################

ENV PWD=/app
ENV DJANGO_DEBUG=FALSE

RUN mkdir -p ${PWD}/sql && \
    chmod -R 777  ${PWD}/sql && \
    mkdir -p ${PWD}/mopseq/uwsgi && \
    chmod -R 777  ${PWD}/mopseq/uwsgi && \
    chmod 777 /var/run && \
    chmod 666 /etc/nginx/conf.d/default.conf && \
    cp ${PWD}/mopseq/mopseq-db.conf /etc/nginx/conf.d/default.conf

#############################################################################
# XPOSE PORT
#############################################################################

EXPOSE 8080/tcp

#############################################################################
# ENTRYPOINT
#############################################################################

STOPSIGNAL SIGTERM
USER $DK_USER
ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
