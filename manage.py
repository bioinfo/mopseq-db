#!/usr/bin/env python
######################################################################################
#MoPSeq-DB database management software.
#Author: Jean-Christophe Mouren, Maude Jacquot
#Creation date: 2021-07-01
#Last modified: 2024-02-23      
######################################################################################
"""Django's command-line utility for administrative tasks."""
import os, typer
import sys

if not str(os.getenv('MPSQ_PROD')) == "True":
    typer.echo("""
######################################################################################
MoPSeq-DB database management software.
Usage: manage.py <subcommand> [Options]         

Most important available subcommands of manage.py:
[features]
    del-pathogen
    update_database

[django]
    check
    loaddata
    makemessages
    makemigrations
    migrate
    optimizemigration

[staticfiles]
    collectstatic
    findstatic
    runserver
          
Type 'manage.py <subcommand>' --help for help on a specific subcommand.
######################################################################################

Running:""")
    print(sys.argv)
    print()

def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mopseq.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)    
  

if __name__ == '__main__':
    main()
 