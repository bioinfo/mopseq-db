# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## In progress

### Added

### Changed

- use TOML file to manage application release numbers 
- above implies use of Poetry tool for development only.
- update docker-compose file by adding missing services directive.

### Fixed

- Issue #15: 
    - upgrade Django version to 4.2.16.

## [1.1.1a0] - from 2024-10-02

Dev in progress for future 1.1.1 release.

## [1.1.0] - 2024-04-05

First public release on production, not changelog file yet.

