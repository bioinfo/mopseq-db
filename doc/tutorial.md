# MoPSeq-DB tutorial

This tutorial explains how to use MoPSeq-DB tools

First of all, you have to declare these environment variables :

```shell
export DOCKER_HOST="127.0.0.1"
export ALLOWED_HOSTS="127.0.0.1"
export MPSQ_PROD="True"
export MPSQ_FILES="/path/to/data_files/"
export MPSQ_BDD="/path/to/mopseq-db-test-data/sql/db.sqlite3"
```

## Update MoPSeq-DB database

To update MoPSeq-DB database, you will need to fill a csv file. You can find a template [here](https://gitlab.ifremer.fr/bioinfo/mopseq-db/-/blob/master/static/other/MoPSeq-DB_template.csv). These commands have to be run in mopseq-db repository and allow modification of both Sqlite3 and Postgres databases.
#### Add genomes
If you want to add new genomes to an existing organism or new genomes from a new organism, you have to use the following command :

```shell
python manage.py update_database /path/to/metadata_file.csv
```
Please fill at least sample_name and kingdom when you add genomes.

#### Update metadata
You have to use same command with the --update option

```shell
python manage.py update_database /path/to/metadata_file.csv --update
```
A genome is find thanks to his sample_name, this field has to appeare in your csv file and you cannot change à sample_name with this command. It only change metadata contain in the csv file, you won't loose potential additional metadata already saved in MoPSeq-DB. If you don't want to change a field, leave it empty in the csv file.

#### Delete genomes
You can also empty the database with the following command :
```shell
python manage.py del-pathogen
```

## Generate visualisation graphs

The following commands need to be run on your computer or in a computing cluster.

#### GFF graphs
##### For linear genomes
```shell
python3 MPSQ-upd.py --pathogen "Ostreid herpesvirus-1" --gff 
```

##### For circular genomes
Install prerequisite software :
```shell
mkdir env 
cd env
# pour la génération des graph pour Vibrio aestu
git clone https://github.com/tonyyzy/GC_analysis.git
git clone https://github.com/jenniferlu717/SkewIT
git clone https://github.com/lh3/minimap2.git
cd minimap2 && make
cd ..
pip install gosling
conda create -p ./ragtag-2.1.0 -c bioconda ragtag=2.1.0
conda activate ragtag-2.1.0
```
To generate data to create a graph with a reference sequence (for partial genomes)
When you generate graph with a reference, graphs for the ref genome are also generated
to generate data to create a graph of only a query and a reference sequence
```shell
python3 BigGenome_gff_image_generator.py -D --reference reference_Name.fasta reference_Name.gff --unique query_Name.fasta query_Name.gff -m /path/to/minimap2_conda_environnement -s /path/to/skewIT_git_folder
```
or, data for several query sequences for one reference sequence
```shell
python3 BigGenome_gff_image_generator.py -D --reference reference_Name.fasta reference_Name.gff --multiple query_list_fasta_gff.csv -m /path/to/minimap2_conda_environnement -s /path/to/skewIT_git_folder
```
Then use the data (after getting a URL for it) to generate graphs 
```shell
python3 BigGenome_gff_image_generator.py -I --url https://url/to/datafiles 
```
Be careful, --url opton has to finish by a / and to NOT include 'to_url_file'
If there is an issue for this step, you can try to reinstall Gosling as it 
```shell
pip install 'gosling[all]==0.0.11'
```
then relaunch the previous command

#### VCF graphs
Only available for Viruses so far :
```shell
python3 MPSQ-upd.py --pathogen "Ostreid herpesvirus-1" --vcf 
```
#### Phylogenetic trees
We advise you to generate the phylogenetic trees from a calculation cluster
##### For viruses
```shell
qsub conda_creation_oshv1_phylotree_tools.pbs
qsub -v path_files=/path/to/fasta_files,path_phylo=/path/to/out_files,jpath=/path/to/jmodeltest /path/to/phylotree.pbs
```
##### For bacteria and protozoa
Instalation of r-base conda environnement
```shell
qsub -I -q ftp -l walltime=01:00:00 -l ncpus=1 -l mem=4g
bash
. /appli/anaconda/latest/etc/profile.d/conda.sh
conda create -p r-base 
conda activate /path/to/r-base
conda install -c bioconda r-base
conda deactivate
```
Then,
```shell
qsub -v files_path=/path/to/fasta_files,matrice_name=matrice_name,out_path=/path/to/out_files,conda_path=/path/to/r-base BigGenome_phylo_pipeline.pbs 
```
##### Results
In both cases, two files must be retrieved and added to mopseq-db-test-data/data-files/organism/phylo: (andi_r_phylo.log or jmodeltest.out) from where we retrieve the information (like the software used, version, etc) to be displayed in MoPSeq-DB and BestModel.tree which contains the Newick format tree.