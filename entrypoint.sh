#!/bin/bash
APP_NAME=mopseq-db
BASEDIR=/app
[[ "_${MPSQ_FILES}" != "_" ]] && export MPSQ_FILES="${MPSQ_FILES}"
[[ "_${MPSQ_BDD}" != "_" ]] && export MPSQ_BDD="${MPSQ_BDD}"
[[ "_${ALLOWED_HOSTS}" != "_" ]] && export ALLOWED_HOSTS="${ALLOWED_HOSTS}"

echo "*** Starting ${APP_NAME} ***"
uwsgi --ini ${PWD}/mopseq/uwsgi.ini &
exec "$@"
